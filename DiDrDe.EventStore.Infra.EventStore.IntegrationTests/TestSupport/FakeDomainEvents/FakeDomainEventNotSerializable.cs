﻿using DiDrDe.CommonContracts;

namespace DiDrDe.EventStore.Infra.EventStore.IntegrationTests.TestSupport.FakeDomainEvents
{
    public class FakeDomainEventNotSerializable
        : IDomainEvent
    {
        public Identity AggregateIdentity { get; }
        public IFakeInterface Foo { get; set; }

        public FakeDomainEventNotSerializable(Identity identity, IFakeInterface foo)
        {
            AggregateIdentity = identity;
            Foo = foo;
        }
    }
}