﻿using DiDrDe.CommonContracts;

namespace DiDrDe.EventStore.Infra.EventStore.IntegrationTests.TestSupport.FakeDomainEvents
{
    public class FakeDomainEvent
        : IDomainEvent
    {
        public Identity AggregateIdentity { get; }

        public FakeDomainEvent(Identity aggregateIdentity)
        {
            AggregateIdentity = aggregateIdentity;
        }
    }
}