﻿using DiDrDe.EventStore.Infra.EventStore.IntegrationTests.TestSupport.FakeDomainEvents;
using DiDrDe.EventStore.Infra.EventStore.IntegrationTests.TestSupport.FakeSerializableEvents;

namespace DiDrDe.EventStore.Infra.EventStore.IntegrationTests.TestSupport.FakeMappers
{
    public class FakeDomainEventNotSerializableToFakeSerializableEventMapper
    {
        public FakeSerializableEvent Map(FakeDomainEventNotSerializable source)
        {
            var aggregateIdentity = source.AggregateIdentity;
            var fakeInterfaceImplementation = (FakeInterfaceImplementation)source.Foo;
            var fakeSerializableEvent =
                new FakeSerializableEvent
                {
                    AggregateIdentity = aggregateIdentity,
                    Foo = fakeInterfaceImplementation
                };
            return fakeSerializableEvent;
        }
    }
}