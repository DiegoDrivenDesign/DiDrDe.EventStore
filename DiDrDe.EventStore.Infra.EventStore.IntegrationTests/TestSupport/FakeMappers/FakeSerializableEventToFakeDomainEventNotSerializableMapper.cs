﻿using DiDrDe.EventStore.Infra.EventStore.IntegrationTests.TestSupport.FakeDomainEvents;
using DiDrDe.EventStore.Infra.EventStore.IntegrationTests.TestSupport.FakeSerializableEvents;

namespace DiDrDe.EventStore.Infra.EventStore.IntegrationTests.TestSupport.FakeMappers
{
    public class FakeSerializableEventToFakeDomainEventNotSerializableMapper
    {
        public FakeDomainEventNotSerializable Map(FakeSerializableEvent source)
        {
            var sourceAggregateIdentity = source.AggregateIdentity;
            var sourceFoo = source.Foo;
            var fakeDomainEventNotSerializable =
                new FakeDomainEventNotSerializable(sourceAggregateIdentity, sourceFoo);
            return fakeDomainEventNotSerializable;
        }
    }
}