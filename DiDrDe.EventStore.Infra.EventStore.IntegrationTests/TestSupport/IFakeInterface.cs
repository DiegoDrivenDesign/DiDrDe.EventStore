﻿namespace DiDrDe.EventStore.Infra.EventStore.IntegrationTests.TestSupport
{
    public interface IFakeInterface
    {
        string Word { get; }
    }
}