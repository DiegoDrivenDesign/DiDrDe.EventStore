﻿namespace DiDrDe.EventStore.Infra.EventStore.IntegrationTests.TestSupport
{
    public class FakeInterfaceImplementation
        : IFakeInterface
    {
        public string Word { get; set; }
        public int Number { get; set; }
    }
}
