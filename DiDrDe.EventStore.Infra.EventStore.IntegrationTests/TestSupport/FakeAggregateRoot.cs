﻿using DiDrDe.CommonContracts;

namespace DiDrDe.EventStore.Infra.EventStore.IntegrationTests.TestSupport
{
    public class FakeAggregateRoot
        : IAggregateRoot
    {
        public Identity AggregateIdentity { get; }
        public int Version { get; }

        public FakeAggregateRoot(Identity aggregateIdentity, int version)
        {
            AggregateIdentity = aggregateIdentity;
            Version = version;
        }
    }
}