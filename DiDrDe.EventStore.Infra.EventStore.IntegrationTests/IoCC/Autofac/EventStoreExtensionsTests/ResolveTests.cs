﻿using System;
using System.Collections.Generic;
using Autofac;
using DiDrDe.CommonContracts;
using DiDrDe.EventStore.Infra.EventStore.Autofac;
using DiDrDe.EventStore.Infra.EventStore.Contracts;
using DiDrDe.EventStore.Infra.EventStore.IntegrationTests.TestSupport;
using DiDrDe.EventStore.Infra.EventStore.IoCC;
using FluentAssertions;
using Moq;
using ToolBelt.TestSupport;
using Xunit;

namespace DiDrDe.EventStore.Infra.EventStore.IntegrationTests.IoCC.Autofac.EventStoreExtensionsTests
{
    public static class ResolveTests
    {
        public class Given_A_Container_With_Event_Store_Registered_When_Resolve_An_IEventStore
            : Given_When_Then_Test
        {
            private IEventStore _sut;
            private IComponentContext _componentContext;

            protected override void Given()
            {
                var builder = new ContainerBuilder();
                builder
                    .RegisterEventStore(
                        ctx =>
                        {
                            var eventStoreOptions =
                                new EventStoreOptions
                                {
                                    ConnectionString = EventStoreTestConstants.TestConnectionString
                                };
                            return eventStoreOptions;
                        });

                var container = builder.Build();
                _componentContext = container.Resolve<IComponentContext>();

            }

            protected override void When()
            {
                _sut = _componentContext.Resolve<IEventStore>();
            }

            [Fact]
            public void Then_It_Should_Not_Be_Null()
            {
                _sut.Should().NotBeNull();
            }
        }

        public class Given_A_Container_With_Event_Store_Registered_When_Resolve_An_IEventStoreManager
            : Given_When_Then_Test
        {
            private IEventStoreManager _sut;
            private IComponentContext _componentContext;

            protected override void Given()
            {
                var builder = new ContainerBuilder();
                builder
                    .RegisterEventStore(
                        ctx =>
                        {
                            var eventStoreOptions =
                                new EventStoreOptions
                                {
                                    ConnectionString = EventStoreTestConstants.TestConnectionString
                                };
                            return eventStoreOptions;
                        });

                var container = builder.Build();
                _componentContext = container.Resolve<IComponentContext>();
            }

            protected override void When()
            {
                _sut = _componentContext.Resolve<IEventStoreManager>();
            }

            [Fact]
            public void Then_It_Should_Not_Be_Null()
            {
                _sut.Should().NotBeNull();
            }
        }

        public class Given_A_Container_With_Event_Store_Readiness_Registered_And_With_Event_Store_Registered_When_Resolve_An_IEventStoreReadiness
            : Given_When_Then_Test
        {
            private IEventStoreReadiness _sut;
            private IComponentContext _componentContext;

            protected override void Given()
            {
                var builder = new ContainerBuilder();
                builder
                    .RegisterEventStore(
                        ctx =>
                        {
                            var eventStoreOptions =
                                new EventStoreOptions
                                {
                                    ConnectionString = EventStoreTestConstants.TestConnectionString
                                };
                            return eventStoreOptions;
                        });
                builder.RegisterEventStoreReadinessCheck();
                var container = builder.Build();
                _componentContext = container.Resolve<IComponentContext>();
            }

            protected override void When()
            {
                _sut = _componentContext.Resolve<IEventStoreReadiness>();
            }

            [Fact]
            public void Then_It_Should_Not_Be_Null()
            {
                _sut.Should().NotBeNull();
            }
        }

        public class Given_A_Container_With_Event_Store_Readiness_Registered_And_Without_Event_Store_Registered_When_Resolve_An_IEventStoreReadiness
            : Given_When_Then_Test
        {
            private IComponentContext _componentContext;
            private Exception _exception;

            protected override void Given()
            {
                var builder = new ContainerBuilder();
                builder.RegisterEventStoreReadinessCheck();
                var container = builder.Build();
                _componentContext = container.Resolve<IComponentContext>();
            }

            protected override void When()
            {
                try
                {
                    _componentContext.Resolve<IEventStoreReadiness>();
                }
                catch (Exception exception)
                {
                    _exception = exception;
                }
            }

            [Fact]
            public void Then_It_Should_Throw_An_Exception()
            {
                _exception.Should().NotBeNull();
            }
        }

        public class Given_A_Container_With_Event_Store_Readiness_Registered_And_Another_IReadiness_When_Resolve_All_The_IReadiness
            : Given_When_Then_Test
        {
            private IEnumerable<IReadiness> _sut;
            private IComponentContext _componentContext;

            protected override void Given()
            {
                var anotherIReadiness = Mock.Of<IReadiness>();
                var builder = new ContainerBuilder();
                builder
                    .RegisterEventStore(
                        ctx =>
                        {
                            var eventStoreOptions =
                                new EventStoreOptions
                                {
                                    ConnectionString = EventStoreTestConstants.TestConnectionString
                                };
                            return eventStoreOptions;
                        });
                builder.RegisterEventStoreReadinessCheck();
                builder
                    .Register(x => anotherIReadiness)
                    .As<IReadiness>();
                var container = builder.Build();
                _componentContext = container.Resolve<IComponentContext>();
            }

            protected override void When()
            {
                _sut = _componentContext.Resolve<IEnumerable<IReadiness>>();
            }

            [Fact]
            public void Then_It_Should_Not_Be_Null()
            {
                _sut.Should().NotBeNull();
            }

            [Fact]
            public void Then_It_Should_Have_Two_Instances()
            {
                _sut.Should().HaveCount(2);
            }
        }
    }
}