﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Autofac;
using DiDrDe.CommonContracts;
using DiDrDe.EventStore.Infra.EventStore.Autofac;
using DiDrDe.EventStore.Infra.EventStore.Contracts;
using DiDrDe.EventStore.Infra.EventStore.Contracts.Factories;
using DiDrDe.EventStore.Infra.EventStore.Exceptions;
using DiDrDe.EventStore.Infra.EventStore.IntegrationTests.TestSupport;
using DiDrDe.EventStore.Infra.EventStore.IntegrationTests.TestSupport.FakeDomainEvents;
using DiDrDe.EventStore.Infra.EventStore.IntegrationTests.TestSupport.FakeMappers;
using DiDrDe.EventStore.Infra.EventStore.IntegrationTests.TestSupport.FakeSerializableEvents;
using DiDrDe.EventStore.Infra.EventStore.IoCC;
using FluentAssertions;
using Moq;
using Newtonsoft.Json;
using ToolBelt.TestSupport;
using Xunit;

namespace DiDrDe.EventStore.Infra.EventStore.IntegrationTests.EventStoreTests
{
    public static class GetEventsTests
    {
        public class Given_An_EventStore_And_A_Stream_Without_Events_When_GetEventsAsync
            : Given_WhenAsync_Then_Test
        {
            private EventStore _sut;
            private Guid _aggregateId;
            private Type _aggregateType;
            private Exception _exception;
            private IEnumerable<IDomainEvent> _result;

            protected override void Given()
            {
                var builder = new ContainerBuilder();
                builder
                    .RegisterEventStore(
                        ctx =>
                        {
                            var eventStoreOptions =
                                new EventStoreOptions
                                {
                                    ConnectionString = EventStoreTestConstants.TestConnectionString
                                };
                            return eventStoreOptions;
                        });

                var container = builder.Build();

                var streamNameFactory = container.Resolve<IStreamNameFactory>();
                var eventStoreRetriever = container.Resolve<IDomainEventsRetriever>();
                var eventStorePersister = container.Resolve<IDomainEventsPersister>();
                var eventStoreManager = container.Resolve<IEventStoreManager>();

                eventStoreManager.StartAsync().GetAwaiter().GetResult();

                _aggregateId = GuidGenerator.Create(1);
                _aggregateType = typeof(FakeAggregateRoot);

                _sut = new EventStore(streamNameFactory, eventStoreRetriever, eventStorePersister);
            }

            protected override async Task WhenAsync()
            {
                try
                {
                    _result = await _sut.GetEvents(_aggregateId, _aggregateType);
                }
                catch (Exception exception)
                {
                    _exception = exception;
                }
            }

            [Fact]
            public void Then_It_Should_Execute_Without_Exception()
            {
                _exception.Should().BeNull();
            }

            [Fact]
            public void Then_It_Should_Return_No_Events()
            {
                _result.Should().HaveCount(0);
            }
        }

        public class Given_An_EventStore_Connection_And_A_Stream_With_One_Event_When_GetEventsAsync
            : Given_WhenAsync_Then_Test
        {
            private EventStore _sut;
            private Guid _aggregateId;
            private Type _aggregateType;
            private Exception _exception;
            private IEnumerable<IDomainEvent> _result;

            protected override void Given()
            {
                var builder = new ContainerBuilder();
                builder
                    .RegisterEventStore(
                        ctx =>
                        {
                            var eventStoreOptions =
                                new EventStoreOptions
                                {
                                    ConnectionString = EventStoreTestConstants.TestConnectionString
                                };
                            return eventStoreOptions;
                        });

                var container = builder.Build();

                var streamNameFactory = container.Resolve<IStreamNameFactory>();
                var eventStoreRetriever = container.Resolve<IDomainEventsRetriever>();
                var eventStorePersister = container.Resolve<IDomainEventsPersister>();
                var eventStoreManager = container.Resolve<IEventStoreManager>();
                eventStoreManager.StartAsync().GetAwaiter().GetResult();

                _aggregateId = GuidGenerator.Create(1);
                _aggregateType = typeof(FakeAggregateRoot);

                var streamName = streamNameFactory.Create(_aggregateId, _aggregateType);
                var testUniqueId = Guid.NewGuid();
                var testStreamName = $"Tests.{testUniqueId}.{streamName}";
                var testStreamNameFactoryMock = new Mock<IStreamNameFactory>();
                testStreamNameFactoryMock
                    .Setup(x => x.Create(_aggregateId, _aggregateType))
                    .Returns(testStreamName);

                var testStreamNameFactory = testStreamNameFactoryMock.Object;

                _sut = new EventStore(testStreamNameFactory, eventStoreRetriever, eventStorePersister);

                var aggregateRootIdentity = new Identity(_aggregateId);

                var domainEvent = new FakeDomainEvent(aggregateRootIdentity);
                var domainEvents =
                    new List<IDomainEvent>
                    {
                        domainEvent
                    };

                var aggregateRoot = new FakeAggregateRoot(aggregateRootIdentity, 1);

                _sut.Persist(aggregateRoot, domainEvents).GetAwaiter().GetResult();
            }

            protected override async Task WhenAsync()
            {
                try
                {
                    _result = await _sut.GetEvents(_aggregateId, _aggregateType);
                }
                catch (Exception exception)
                {
                    _exception = exception;
                }
            }

            [Fact]
            public void Then_It_Should_Execute_Without_Exception()
            {
                _exception.Should().BeNull();
            }

            [Fact]
            public void Then_It_Should_Return_One_Event()
            {
                _result.Should().HaveCount(1);
            }
        }

        public class Given_An_EventStore_Connection_And_A_Stream_With_Three_Events_When_GetEventsAsync
            : Given_WhenAsync_Then_Test
        {
            private EventStore _sut;
            private Guid _aggregateId;
            private Type _aggregateType;
            private List<IDomainEvent> _domainEvents;
            private Exception _exception;
            private IEnumerable<IDomainEvent> _result;

            protected override void Given()
            {
                var builder = new ContainerBuilder();
                builder
                    .RegisterEventStore(
                        ctx =>
                        {
                            var eventStoreOptions =
                                new EventStoreOptions
                                {
                                    ConnectionString = EventStoreTestConstants.TestConnectionString
                                };
                            return eventStoreOptions;
                        });

                var container = builder.Build();

                var streamNameFactory = container.Resolve<IStreamNameFactory>();
                var eventStoreRetriever = container.Resolve<IDomainEventsRetriever>();
                var eventStorePersister = container.Resolve<IDomainEventsPersister>();
                var eventStoreManager = container.Resolve<IEventStoreManager>();
                eventStoreManager.StartAsync().GetAwaiter().GetResult();

                _aggregateId = GuidGenerator.Create(1);
                _aggregateType = typeof(FakeAggregateRoot);

                var streamName = streamNameFactory.Create(_aggregateId, _aggregateType);
                var testUniqueId = Guid.NewGuid();
                var testStreamName = $"Tests.{testUniqueId}.{streamName}";
                var testStreamNameFactoryMock = new Mock<IStreamNameFactory>();
                testStreamNameFactoryMock
                    .Setup(x => x.Create(_aggregateId, _aggregateType))
                    .Returns(testStreamName);

                var testStreamNameFactory = testStreamNameFactoryMock.Object;

                _sut = new EventStore(testStreamNameFactory, eventStoreRetriever, eventStorePersister);

                var aggregateRootIdentity = new Identity(_aggregateId);

                var domainEventOne = new FakeDomainEvent(aggregateRootIdentity);
                var domainEventTwo = new FakeDomainEvent(aggregateRootIdentity);
                var domainEventThree = new FakeDomainEvent(aggregateRootIdentity);
                _domainEvents =
                    new List<IDomainEvent>
                    {
                        domainEventOne,
                        domainEventTwo,
                        domainEventThree
                    };

                var aggregateRoot = new FakeAggregateRoot(aggregateRootIdentity, 3);

                _sut.Persist(aggregateRoot, _domainEvents).GetAwaiter().GetResult();
            }

            protected override async Task WhenAsync()
            {
                try
                {
                    _result = await _sut.GetEvents(_aggregateId, _aggregateType);
                }
                catch (Exception exception)
                {
                    _exception = exception;
                }
            }

            [Fact]
            public void Then_It_Should_Execute_Without_Exception()
            {
                _exception.Should().BeNull();
            }


            [Fact]
            public void Then_It_Should_Return_Three_Events()
            {
                _result.Should().HaveCount(3);
            }

            [Fact]
            public void Then_It_Should_Return_The_Same_Domain_Event_That_Was_Saved_Previously()
            {
                _result.Should().BeEquivalentTo(_domainEvents);
            }
        }

        public class Given_An_Event_Store_With_A_Non_Serializable_Domain_Event_And_No_Custom_Mapper_Configured_When_GetEventsAsync
            : Given_WhenAsync_Then_Test
        {
            private EventStore _sut;
            private Guid _aggregateId;
            private IAggregateRoot _aggregateRoot;
            private CouldNotRetrieveEventsException _exception;
            private Type _aggregateRootType;
            private IEnumerable<IDomainEvent> _domainEvents;
            private IEnumerable<IDomainEvent> _result;

            protected override void Given()
            {
                var builder = new ContainerBuilder();
                builder
                    .RegisterEventStore(
                        ctx =>
                        {
                            var eventStoreOptions =
                                new EventStoreOptions
                                {
                                    ConnectionString = EventStoreTestConstants.TestConnectionString
                                };
                            return eventStoreOptions;
                        });

                var container = builder.Build();

                var streamNameFactory = container.Resolve<IStreamNameFactory>();
                var eventStoreRetriever = container.Resolve<IDomainEventsRetriever>();
                var eventStorePersister = container.Resolve<IDomainEventsPersister>();
                var eventStoreManager = container.Resolve<IEventStoreManager>();

                eventStoreManager.StartAsync().GetAwaiter().GetResult();

                _aggregateId = GuidGenerator.Create(1);
                var aggregateRootIdentity = new Identity(_aggregateId);

                var aggregateType = typeof(FakeAggregateRoot);
                var streamName = streamNameFactory.Create(_aggregateId, aggregateType);
                var testUniqueId = Guid.NewGuid();
                var testStreamName = $"Tests.{testUniqueId}.{streamName}";
                var testStreamNameFactoryMock = new Mock<IStreamNameFactory>();
                testStreamNameFactoryMock
                    .Setup(x => x.Create(_aggregateId, aggregateType))
                    .Returns(testStreamName);

                var testStreamNameFactory = testStreamNameFactoryMock.Object;

                var interfaceImplementation =
                    new FakeInterfaceImplementation
                    {
                        Word = "bar",
                        Number = 9
                    };

                var domainEventOne = new FakeDomainEventNotSerializable(aggregateRootIdentity, interfaceImplementation);
                _domainEvents =
                    new List<IDomainEvent>
                    {
                        domainEventOne
                    };

                _aggregateRoot = new FakeAggregateRoot(aggregateRootIdentity, 1);
                _aggregateRootType = _aggregateRoot.GetType();

                _sut = new EventStore(testStreamNameFactory, eventStoreRetriever, eventStorePersister);

                _sut.Persist(_aggregateRoot, _domainEvents).GetAwaiter().GetResult();
            }

            protected override async Task WhenAsync()
            {
                try
                {
                    _result = await _sut.GetEvents(_aggregateId, _aggregateRootType);
                }
                catch (CouldNotRetrieveEventsException exception)
                {
                    _exception = exception;
                }
            }

            [Fact]
            public void Then_It_Should_Return_Null()
            {
                _result.Should().BeNull();
            }

            [Fact]
            public void Then_It_Should_Throw_A_CouldNotRetrieveEventsException()
            {
                _exception.Should().NotBeNull();
            }

            [Fact]
            public void Then_It_Should_Have_An_Exception_Containing_A_JsonSerializationException()
            {
                _exception.InnerException.Should().BeAssignableTo<JsonSerializationException>();
            }
        }

        public class Given_An_Event_Store_With_A_Non_Serializable_Domain_Event_And_A_Custom_Mapper_Configured_When_GetEventsAsync
            : Given_WhenAsync_Then_Test
        {
            private EventStore _sut;
            private Guid _aggregateId;
            private IAggregateRoot _aggregateRoot;
            private Exception _exception;
            private Type _aggregateRootType;
            private IEnumerable<IDomainEvent> _domainEvents;
            private IEnumerable<IDomainEvent> _result;

            protected override void Given()
            {
                var builder = new ContainerBuilder();
                builder
                    .RegisterEventStore(
                        ctx =>
                        {
                            var eventStoreOptions =
                                new EventStoreOptions
                                {
                                    ConnectionString = EventStoreTestConstants.TestConnectionString
                                };
                            return eventStoreOptions;
                        },
                        ctx =>
                        {
                            var customDomainEventMappersOptions =
                                new CustomDomainEventMappersOptions()
                                    .UsesCustomMappers<FakeDomainEventNotSerializable, FakeSerializableEvent>(
                                        domainEvent =>
                                        {
                                            var mapper =
                                                new FakeDomainEventNotSerializableToFakeSerializableEventMapper();
                                            var result = mapper.Map(domainEvent);
                                            return result;
                                        },
                                        serializableEvent =>
                                        {
                                            var mapper =
                                                new FakeSerializableEventToFakeDomainEventNotSerializableMapper();
                                            var result = mapper.Map(serializableEvent);
                                            return result;
                                        });
                            return customDomainEventMappersOptions;
                        });

                var container = builder.Build();

                var streamNameFactory = container.Resolve<IStreamNameFactory>();
                var eventStoreRetriever = container.Resolve<IDomainEventsRetriever>();
                var eventStorePersister = container.Resolve<IDomainEventsPersister>();
                var eventStoreManager = container.Resolve<IEventStoreManager>();

                eventStoreManager.StartAsync().GetAwaiter().GetResult();

                _aggregateId = GuidGenerator.Create(1);
                var aggregateRootIdentity = new Identity(_aggregateId);

                var aggregateType = typeof(FakeAggregateRoot);
                var streamName = streamNameFactory.Create(_aggregateId, aggregateType);
                var testUniqueId = Guid.NewGuid();
                var testStreamName = $"Tests.{testUniqueId}.{streamName}";
                var testStreamNameFactoryMock = new Mock<IStreamNameFactory>();
                testStreamNameFactoryMock
                    .Setup(x => x.Create(_aggregateId, aggregateType))
                    .Returns(testStreamName);

                var testStreamNameFactory = testStreamNameFactoryMock.Object;

                var interfaceImplementation =
                    new FakeInterfaceImplementation
                    {
                        Word = "bar",
                        Number = 9
                    };

                var domainEventNotSerializable = new FakeDomainEventNotSerializable(aggregateRootIdentity, interfaceImplementation);
                _domainEvents =
                    new List<IDomainEvent>
                    {
                        domainEventNotSerializable
                    };

                _aggregateRoot = new FakeAggregateRoot(aggregateRootIdentity, 1);
                _aggregateRootType = _aggregateRoot.GetType();

                _sut = new EventStore(testStreamNameFactory, eventStoreRetriever, eventStorePersister);

                _sut.Persist(_aggregateRoot, _domainEvents).GetAwaiter().GetResult();
            }

            protected override async Task WhenAsync()
            {
                try
                {
                    _result = await _sut.GetEvents(_aggregateId, _aggregateRootType);
                }
                catch (Exception exception)
                {
                    _exception = exception;
                }
            }

            [Fact]
            public void Then_It_Should_Execute_Without_Exceptions()
            {
                _exception.Should().BeNull();
            }

            [Fact]
            public void Then_It_Should_Return_One_Event()
            {
                _result.Should().HaveCount(1);
            }

            [Fact]
            public void Then_It_Should_Return_The_Same_Domain_Event_That_Was_Saved_Previously()
            {
                _result.Should().BeEquivalentTo(_domainEvents);
            }
        }
    }
}