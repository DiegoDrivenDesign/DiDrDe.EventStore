﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Autofac;
using DiDrDe.CommonContracts;
using DiDrDe.EventStore.Infra.EventStore.Autofac;
using DiDrDe.EventStore.Infra.EventStore.Contracts;
using DiDrDe.EventStore.Infra.EventStore.Contracts.Factories;
using DiDrDe.EventStore.Infra.EventStore.IntegrationTests.TestSupport;
using DiDrDe.EventStore.Infra.EventStore.IntegrationTests.TestSupport.FakeDomainEvents;
using DiDrDe.EventStore.Infra.EventStore.IoCC;
using FluentAssertions;
using Moq;
using ToolBelt.TestSupport;
using Xunit;

namespace DiDrDe.EventStore.Infra.EventStore.IntegrationTests.EventStoreTests
{
    public static class PersistTests
    {
        public class Given_An_Event_Store_And_A_Domain_Event_When_PersistAsync
            : Given_WhenAsync_Then_Test
        {
            private EventStore _sut;
            private Guid _aggregateId;
            private IAggregateRoot _aggregateRoot;
            private IEnumerable<IDomainEvent> _domainEvents;
            private Exception _exception;

            protected override void Given()
            {
                var builder = new ContainerBuilder();
                builder
                    .RegisterEventStore(
                        ctx =>
                        {
                            var eventStoreOptions =
                                new EventStoreOptions
                                {
                                    ConnectionString = EventStoreTestConstants.TestConnectionString
                                };
                            return eventStoreOptions;
                        });

                var container = builder.Build();

                var streamNameFactory = container.Resolve<IStreamNameFactory>();
                var eventStoreRetriever = container.Resolve<IDomainEventsRetriever>();
                var eventStorePersister = container.Resolve<IDomainEventsPersister>();
                var eventStoreManager = container.Resolve<IEventStoreManager>();

                eventStoreManager.StartAsync().GetAwaiter().GetResult();

                _aggregateId = GuidGenerator.Create(1);
                var aggregateRootIdentity = new Identity(_aggregateId);

                var aggregateType = typeof(FakeAggregateRoot);
                var streamName = streamNameFactory.Create(_aggregateId, aggregateType);
                var testUniqueId = Guid.NewGuid();
                var testStreamName = $"Tests.{testUniqueId}.{streamName}";
                var testStreamNameFactoryMock = new Mock<IStreamNameFactory>();
                testStreamNameFactoryMock
                    .Setup(x => x.Create(_aggregateId, aggregateType))
                    .Returns(testStreamName);

                var testStreamNameFactory = testStreamNameFactoryMock.Object;

                var domainEvent = new FakeDomainEvent(aggregateRootIdentity);
                _domainEvents =
                    new List<IDomainEvent>
                    {
                        domainEvent
                    };

                var aggregateRootVersion = _domainEvents.Count();
                _aggregateRoot = new FakeAggregateRoot(aggregateRootIdentity, aggregateRootVersion);

                _sut = new EventStore(testStreamNameFactory, eventStoreRetriever, eventStorePersister);
            }

            protected override async Task WhenAsync()
            {
                try
                {
                    await _sut.Persist(_aggregateRoot, _domainEvents);
                }
                catch (Exception exception)
                {
                    _exception = exception;
                }
            }

            [Fact]
            public void Then_It_Should_Execute_Without_Exception()
            {
                _exception.Should().BeNull();
            }
        }

        public class Given_An_Event_Store_And_A_Non_Serializable_Domain_Event_When_PersistAsync
            : Given_WhenAsync_Then_Test
        {
            private EventStore _sut;
            private IAggregateRoot _aggregateRoot;
            private IEnumerable<IDomainEvent> _domainEvents;
            private Exception _exception;

            protected override void Given()
            {
                var builder = new ContainerBuilder();
                builder
                    .RegisterEventStore(
                        ctx =>
                        {
                            var eventStoreOptions =
                                new EventStoreOptions
                                {
                                    ConnectionString = EventStoreTestConstants.TestConnectionString
                                };
                            return eventStoreOptions;
                        });

                var container = builder.Build();

                var streamNameFactory = container.Resolve<IStreamNameFactory>();
                var eventStoreRetriever = container.Resolve<IDomainEventsRetriever>();
                var eventStorePersister = container.Resolve<IDomainEventsPersister>();
                var eventStoreManager = container.Resolve<IEventStoreManager>();

                eventStoreManager.StartAsync().GetAwaiter().GetResult();

                var aggregateId = GuidGenerator.Create(1);
                var aggregateRootIdentity = new Identity(aggregateId);

                var aggregateType = typeof(FakeAggregateRoot);
                var streamName = streamNameFactory.Create(aggregateId, aggregateType);
                var testUniqueId = Guid.NewGuid();
                var testStreamName = $"Tests.{testUniqueId}.{streamName}";
                var testStreamNameFactoryMock = new Mock<IStreamNameFactory>();
                testStreamNameFactoryMock
                    .Setup(x => x.Create(aggregateId, aggregateType))
                    .Returns(testStreamName);

                var testStreamNameFactory = testStreamNameFactoryMock.Object;

                var foo =
                    new FakeInterfaceImplementation
                    {
                        Word = "foo"
                    };
                var domainEvent = new FakeDomainEventNotSerializable(aggregateRootIdentity, foo);
                _domainEvents =
                    new List<IDomainEvent>
                    {
                        domainEvent
                    };

                var aggregateRootVersion = _domainEvents.Count();
                _aggregateRoot = new FakeAggregateRoot(aggregateRootIdentity, aggregateRootVersion);

                _sut = new EventStore(testStreamNameFactory, eventStoreRetriever, eventStorePersister);
            }

            protected override async Task WhenAsync()
            {
                try
                {
                    await _sut.Persist(_aggregateRoot, _domainEvents);
                }
                catch (Exception exception)
                {
                    _exception = exception;
                }
            }

            [Fact]
            public void Then_It_Should_Execute_Without_Exception()
            {
                _exception.Should().BeNull();
            }
        }
    }
}