﻿namespace DiDrDe.CommonContracts
{
    public interface IAggregateRoot
    {
        Identity AggregateIdentity { get; }

        int Version { get; }
    }
}
