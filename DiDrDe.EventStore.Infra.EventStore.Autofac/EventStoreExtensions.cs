﻿using Autofac;
using DiDrDe.CommonContracts;
using DiDrDe.EventStore.Infra.EventStore.Autofac.Configuration;
using DiDrDe.EventStore.Infra.EventStore.Contracts;
using DiDrDe.EventStore.Infra.EventStore.Contracts.Configuration;
using DiDrDe.EventStore.Infra.EventStore.Contracts.Converters;
using DiDrDe.EventStore.Infra.EventStore.Contracts.Factories;
using DiDrDe.EventStore.Infra.EventStore.Contracts.Repositories;
using DiDrDe.EventStore.Infra.EventStore.Converters;
using DiDrDe.EventStore.Infra.EventStore.Factories;
using DiDrDe.EventStore.Infra.EventStore.IoCC;
using DiDrDe.EventStore.Infra.EventStore.Repositories;
using EventStore.ClientAPI;
using System;
using System.Linq;
using System.Text;

namespace DiDrDe.EventStore.Infra.EventStore.Autofac
{
    public static class EventStoreExtensions
    {
        public static void RegisterEventStore(
            this ContainerBuilder builder,
            Func<IComponentContext, EventStoreOptions> optionsRetriever,
            Func<IComponentContext, CustomDomainEventMappersOptions> customDomainEventMappersOptionsRetriever = null)
        {
            RegisterEventStoreConnection(builder, optionsRetriever);
            RegisterFactories(builder);
            RegisterEventStoreManager(builder);
            RegisterConverters(builder);
            RegisterResolvers(builder, customDomainEventMappersOptionsRetriever);
            RegisterEventStoreServices(builder);
        }

        public static void RegisterEventStoreReadinessCheck(
            this ContainerBuilder builder)
        {
            builder
                .RegisterType<EventStoreReadiness>()
                .As<IEventStoreReadiness>()
                .As<IReadiness>()
                .SingleInstance();
        }

        private static void RegisterEventStoreConnection(
            ContainerBuilder builder,
            Func<IComponentContext, EventStoreOptions> optionsRetriever)
        {
            builder
                .Register(context =>
                {
                    var ctx = context.Resolve<IComponentContext>();
                    var eventStoreOptions = optionsRetriever.Invoke(ctx);
                    var connectionString = eventStoreOptions.ConnectionString;
                    var connectionSettingsBuilder =
                        ConnectionSettings
                            .Create()
                            .KeepReconnecting()
                            .SetMaxDiscoverAttempts(int.MaxValue);
                    var eventStoreConnection = EventStoreConnection.Create(connectionString, connectionSettingsBuilder, "EventStoreConnection");
                    eventStoreConnection.Disconnected +=
                        (sender, args) =>
                        {
                            Console.Out.WriteLine($"Event Store Connection {args.Connection.ConnectionName} has been Disconnected from {args.RemoteEndPoint.Address}");
                        };
                    eventStoreConnection.Closed +=
                        (sender, args) =>
                        {
                            Console.Out.WriteLine($"Event Store Connection {args.Connection.ConnectionName} has been Closed. Reason {args.Reason}");
                        };
                    eventStoreConnection.Connected +=
                        (sender, args) =>
                        {
                            Console.Out.WriteLine($"Event Store Connection {args.Connection.ConnectionName} is now Connected on {args.RemoteEndPoint.Address}");
                        };
                    return eventStoreConnection;
                })
                .As<IEventStoreConnection>()
                .SingleInstance();

            builder
                .Register(
                    context =>
                    {
                        var ctx = context.Resolve<IComponentContext>();
                        var eventStoreOptions = optionsRetriever.Invoke(ctx);
                        var timeoutTimeSpan = eventStoreOptions.TimeoutTimeSpan;
                        if (timeoutTimeSpan.Milliseconds <= default(TimeSpan).Milliseconds)
                        {
                            timeoutTimeSpan = TimeSpan.FromMinutes(1);
                        }
                        var timeoutConfiguration = new TimeoutConfiguration(timeoutTimeSpan);
                        return timeoutConfiguration;
                    })
                .As<ITimeoutConfiguration>()
                .SingleInstance();
        }

        private static void RegisterFactories(
            ContainerBuilder builder)
        {
            builder
                .RegisterType<DomainEventFactory>()
                .As<IDomainEventFactory>()
                .SingleInstance();

            builder
                .RegisterType<EventDataFactory>()
                .As<IEventDataFactory>()
                .SingleInstance();

            builder
                .RegisterType<ExpectedVersionFactory>()
                .As<IExpectedVersionFactory>()
                .SingleInstance();

            builder
                .RegisterType<IdFactory>()
                .As<IIdFactory>()
                .SingleInstance();

            builder
                .RegisterType<StreamNameFactory>()
                .As<IStreamNameFactory>()
                .SingleInstance();

            builder
                .RegisterType<RetrievedEventFactory>()
                .As<IRetrievedEventFactory>()
                .SingleInstance();
        }

        private static void RegisterEventStoreManager(ContainerBuilder builder)
        {
            builder
                .RegisterType<EventStoreManager>()
                .As<IEventStoreManager>()
                .SingleInstance();
        }

        private static void RegisterConverters(ContainerBuilder builder)
        {
            builder
                .Register(x =>
                {
                    var utf8Encoding = new BytesConverter(Encoding.UTF8);
                    return utf8Encoding;
                })
                .As<IBytesConverter>()
                .SingleInstance();

            builder
                .RegisterType<NewtonsoftConverter>()
                .As<IJsonConverter>()
                .SingleInstance();
        }

        private static void RegisterResolvers(
            ContainerBuilder builder,
            Func<IComponentContext, CustomDomainEventMappersOptions> customDomainEventMappersOptionsRetriever)
        {
            builder
                .Register(context =>
                {
                    var ctx = context.Resolve<IComponentContext>();
                    var customDomainEventMappersOptions = customDomainEventMappersOptionsRetriever?.Invoke(ctx);
                    var domainEventSerializerDeserializers =
                        customDomainEventMappersOptions?.DomainEventSerializerDeserializers;
                    var mapperResolver = MapperResolverFactory.Create(domainEventSerializerDeserializers);
                    return mapperResolver;
                })
                .As<IMapperResolver>()
                .SingleInstance();

            builder
                .Register(context =>
                {
                    var ctx = context.Resolve<IComponentContext>();

                    var assembliesLoaded = AppDomain.CurrentDomain.GetAssemblies();
                    var domainEventTypes =
                        assembliesLoaded
                            .SelectMany(s => s.GetTypes())
                            .Where(x => typeof(IDomainEvent).IsAssignableFrom(x)
                                        && x.IsClass);
                    var customDomainEventMappersOptions = customDomainEventMappersOptionsRetriever?.Invoke(ctx);
                    var domainEventSerializerDeserializers =
                        customDomainEventMappersOptions?.DomainEventSerializerDeserializers;
                    var typeResolver =
                        EventTypeResolverFactory.Create(
                            domainEventTypes,
                            domainEventSerializerDeserializers);
                    return typeResolver;
                })
                .As<IEventTypeResolver>()
                .SingleInstance();
        }

        private static void RegisterEventStoreServices(ContainerBuilder builder)
        {
            builder
                .RegisterType<EventStoreRepository>()
                .As<IEventStoreRepository>();

            builder
                .RegisterType<DomainEventsPersister>()
                .As<IDomainEventsPersister>();

            builder
                .RegisterType<DomainEventsRetriever>()
                .As<IDomainEventsRetriever>();

            builder
                .RegisterType<EventStore>()
                .As<IEventStore>();
        }
    }
}