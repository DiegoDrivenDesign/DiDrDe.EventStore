﻿using System;
using DiDrDe.EventStore.Infra.EventStore.Contracts.Configuration;

namespace DiDrDe.EventStore.Infra.EventStore.Autofac.Configuration
{
    public class TimeoutConfiguration
        : ITimeoutConfiguration
    {
        public TimeSpan TimeSpan { get; }

        public TimeoutConfiguration(TimeSpan timeSpan)
        {
            TimeSpan = timeSpan;
        }
    }
}
