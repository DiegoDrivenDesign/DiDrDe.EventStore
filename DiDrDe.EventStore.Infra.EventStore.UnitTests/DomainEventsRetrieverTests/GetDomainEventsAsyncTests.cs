﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using DiDrDe.CommonContracts;
using DiDrDe.EventStore.Infra.EventStore.Contracts;
using DiDrDe.EventStore.Infra.EventStore.Contracts.Factories;
using DiDrDe.EventStore.Infra.EventStore.Contracts.Repositories;
using DiDrDe.EventStore.Infra.EventStore.Exceptions;
using DiDrDe.EventStore.Infra.EventStore.UnitTests.TestSupport.Builders;
using FluentAssertions;
using Moq;
using ToolBelt.TestSupport;
using Xunit;

namespace DiDrDe.EventStore.Infra.EventStore.UnitTests.DomainEventsRetrieverTests
{
    public static class GetDomainEventsAsyncTests
    {
        public class Given_An_Event_Store_Repository_With_Three_Retrieve_Events_And_A_Stream_Name_When_GetEventsAsync
            : Given_WhenAsync_Then_Test
        {
            private DomainEventsRetriever _sut;
            private string _streamName;
            private Mock<IDomainEventFactory> _domainEventFactoryMock;
            private Mock<IEventStoreRepository> _eventStoreRepositoryMock;
            private IEnumerable<IDomainEvent> _result;
            private Exception _exception;

            protected override void Given()
            {
                _streamName = "foo";
                _domainEventFactoryMock = new Mock<IDomainEventFactory>();
                var domainEventFactory = _domainEventFactoryMock.Object;
                _eventStoreRepositoryMock = new Mock<IEventStoreRepository>();
                var eventOne = Mock.Of<IRetrievedEvent>();
                var eventTwo = Mock.Of<IRetrievedEvent>();
                var eventThree = Mock.Of<IRetrievedEvent>();
                var resolvedEvents =
                    new List<IRetrievedEvent>()
                    {
                        eventOne,
                        eventTwo,
                        eventThree
                    };
                _eventStoreRepositoryMock
                    .Setup(x => x.Get(_streamName, It.IsAny<int>(), It.IsAny<int>(), It.IsAny<bool>()))
                    .ReturnsAsync(resolvedEvents);
                var eventStoreRepository = _eventStoreRepositoryMock.Object;
                _sut =
                    new EventStoreRetrieverBuilder()
                        .WithDomainEventFactory(domainEventFactory)
                        .WithEventStoreRepository(eventStoreRepository)
                        .Build();
            }

            protected override async Task WhenAsync()
            {
                try
                {
                    _result = await _sut.GetDomainEventsAsync(_streamName);
                }
                catch (Exception exception)
                {
                    _exception = exception;
                }
            }

            [Fact]
            public void Then_It_Should_Not_Throw_Any_Exception()
            {
                _exception.Should().BeNull();
            }

            [Fact]
            public void Then_It_Should_Get_Events_From_The_Event_Store_Repository()
            {
                _eventStoreRepositoryMock.Verify(x => x.Get(_streamName, It.IsAny<int>(), It.IsAny<int>(), It.IsAny<bool>()));
            }

            [Fact]
            public void Then_It_Should_Create_Three_Domain_Events()
            {
                _domainEventFactoryMock.Verify(x => x.Create(It.IsAny<IRetrievedEvent>()), Times.Exactly(3));
            }
        }

        public class Given_A_Problem_In_The_Event_Store_Repository_And_A_Stream_Name_When_GetEventsAsync
            : Given_WhenAsync_Then_Test
        {
            private DomainEventsRetriever _sut;
            private string _streamName;
            private IEnumerable<IDomainEvent> _result;
            private CouldNotRetrieveEventsException _exception;

            protected override void Given()
            {
                _streamName = "foo";
                var eventStoreRepositoryMock = new Mock<IEventStoreRepository>();
                eventStoreRepositoryMock
                    .Setup(x => x.Get(_streamName, It.IsAny<int>(), It.IsAny<int>(), It.IsAny<bool>()))
                    .Throws<Exception>();
                var eventStoreRepository = eventStoreRepositoryMock.Object;
                _sut =
                    new EventStoreRetrieverBuilder()
                        .WithEventStoreRepository(eventStoreRepository)
                        .Build();
            }

            protected override async Task WhenAsync()
            {
                try
                {
                    _result = await _sut.GetDomainEventsAsync(_streamName);
                }
                catch (CouldNotRetrieveEventsException exception)
                {
                    _exception = exception;
                }
            }

            [Fact]
            public void Then_It_Return_Null()
            {
                _result.Should().BeNull();
            }

            [Fact]
            public void Then_It_Should_Throw_A_CouldNotRetrieveEventsException()
            {
                _exception.Should().NotBeNull();
            }
        }
    }
}