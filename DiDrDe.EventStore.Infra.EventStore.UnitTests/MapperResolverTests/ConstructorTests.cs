﻿using System;
using System.Collections.Generic;
using DiDrDe.EventStore.Infra.EventStore.Contracts;
using DiDrDe.EventStore.Infra.EventStore.IoCC;
using FluentAssertions;
using Moq;
using ToolBelt.TestSupport;
using Xunit;

namespace DiDrDe.EventStore.Infra.EventStore.UnitTests.MapperResolverTests
{
    public static class ConstructorTests
    {
        public class Given_Valid_Dependencies_When_Constructing_Instance
            : Given_When_Then_Test
        {
            private MapperResolver _sut;
            private IReadOnlyDictionary<Type, DomainEventSerializerDeserializer> _domainEventSerializerDeserializers;

            protected override void Given()
            {
                _domainEventSerializerDeserializers =
                    Mock.Of<IReadOnlyDictionary<Type, DomainEventSerializerDeserializer>>();
            }

            protected override void When()
            {
                _sut = new MapperResolver(_domainEventSerializerDeserializers);
            }

            [Fact]
            public void Then_It_Should_Have_Created_A_Valid_Instance()
            {
                _sut.Should().NotBeNull();
            }

            [Fact]
            public void Then_It_Should_Be_An_IMapperResolver()
            {
                _sut.Should().BeAssignableTo<IMapperResolver>();
            }
        }

        public class Given_Null_Dependencies_When_Constructing_Instance
            : Given_When_Then_Test
        {
            private MapperResolver _sut;
            private IReadOnlyDictionary<Type, DomainEventSerializerDeserializer> _domainEventSerializerDeserializers;
            private ArgumentNullException _exception;

            protected override void Given()
            {
                _domainEventSerializerDeserializers = null;
            }

            protected override void When()
            {
                try
                {
                    _sut = new MapperResolver(_domainEventSerializerDeserializers);
                }
                catch (ArgumentNullException exception)
                {
                    _exception = exception;
                }
            }

            [Fact]
            public void Then_It_Should_Be_Null()
            {
                _sut.Should().BeNull();
            }

            [Fact]
            public void Then_It_Should_Throw_An_ArgumentNullException()
            {
                _exception.Should().NotBeNull();
            }
        }
    }
}