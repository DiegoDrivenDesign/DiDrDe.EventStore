﻿using DiDrDe.CommonContracts;
using DiDrDe.EventStore.Infra.EventStore.Contracts;
using EventStore.ClientAPI;
using FluentAssertions;
using Moq;
using ToolBelt.TestSupport;
using Xunit;

namespace DiDrDe.EventStore.Infra.EventStore.UnitTests.EventStoreReadinessTests
{
    public static class ConstructorTests
    {
        public class Given_Valid_Dependencies_When_Constructing_Instance
            : Given_When_Then_Test
        {
            private EventStoreReadiness _sut;
            private IEventStoreConnection _eventStoreConnection;

            protected override void Given()
            {
                _eventStoreConnection = Mock.Of<IEventStoreConnection>();
            }

            protected override void When()
            {
                _sut = new EventStoreReadiness(_eventStoreConnection);
            }

            [Fact]
            public void Then_It_Should_Have_Created_A_Valid_Instance()
            {
                _sut.Should().NotBeNull();
            }

            [Fact]
            public void Then_It_Should_Be_An_IEventStoreReadiness()
            {
                _sut.Should().BeAssignableTo<IEventStoreReadiness>();
            }

            [Fact]
            public void Then_It_Should_Be_An_IReadiness()
            {
                _sut.Should().BeAssignableTo<IReadiness>();
            }
        }
    }
}