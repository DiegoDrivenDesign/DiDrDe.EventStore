﻿using DiDrDe.EventStore.Infra.EventStore.Contracts;
using DiDrDe.EventStore.Infra.EventStore.Contracts.Factories;
using FluentAssertions;
using Moq;
using ToolBelt.TestSupport;
using Xunit;

namespace DiDrDe.EventStore.Infra.EventStore.UnitTests.EventStoreTests
{
    public static class ConstructorTests
    {
        public class Given_Valid_Dependencies_When_Constructing_Instance
            : Given_When_Then_Test
        {
            private EventStore _sut;
            private IStreamNameFactory _streamNameFactory;
            private IDomainEventsRetriever _eventStoreRetriever;
            private IDomainEventsPersister _eventStorePersister;

            protected override void Given()
            {
                _streamNameFactory = Mock.Of<IStreamNameFactory>();
                _eventStoreRetriever = Mock.Of<IDomainEventsRetriever>();
                _eventStorePersister = Mock.Of<IDomainEventsPersister>();
            }

            protected override void When()
            {
                _sut = new EventStore(_streamNameFactory, _eventStoreRetriever, _eventStorePersister);
            }

            [Fact]
            public void Then_It_Should_Have_Created_A_Valid_Instance()
            {
                _sut.Should().NotBeNull();
            }

            [Fact]
            public void Then_It_Should_Be_An_IEventStore()
            {
                _sut.Should().BeAssignableTo<IEventStore>();
            }
        }
    }
}