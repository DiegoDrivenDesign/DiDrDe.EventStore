﻿using DiDrDe.EventStore.Infra.EventStore.Contracts;
using EventStore.ClientAPI;
using FluentAssertions;
using ToolBelt.TestSupport;
using Xunit;

namespace DiDrDe.EventStore.Infra.EventStore.UnitTests.RetrievedEventTests
{
    public static class ConstructorTests
    {
        public class Given_Valid_Dependencies_When_Constructing_Instance
            : Given_When_Then_Test
        {
            private RetrievedEvent _sut;
            private ResolvedEvent _resolvedEvent;

            protected override void Given()
            {
                _resolvedEvent = new ResolvedEvent();
            }

            protected override void When()
            {
                _sut = new RetrievedEvent(_resolvedEvent);
            }

            [Fact]
            public void Then_It_Should_Have_Created_A_Valid_Instance()
            {
                _sut.Should().NotBeNull();
            }

            [Fact]
            public void Then_It_Should_Be_An_IRetrievedEvent()
            {
                _sut.Should().BeAssignableTo<IRetrievedEvent>();
            }
        }
    }
}