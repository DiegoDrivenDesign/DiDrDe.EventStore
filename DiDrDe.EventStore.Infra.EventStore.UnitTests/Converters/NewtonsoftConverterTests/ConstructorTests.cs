﻿using DiDrDe.EventStore.Infra.EventStore.Contracts.Converters;
using DiDrDe.EventStore.Infra.EventStore.Converters;
using FluentAssertions;
using ToolBelt.TestSupport;
using Xunit;

namespace DiDrDe.EventStore.Infra.EventStore.UnitTests.Converters.NewtonsoftConverterTests
{
    public static class ConstructorTests
    {
        public class Given_Valid_Dependencies_When_Constructing_Instance
            : Given_When_Then_Test
        {
            private NewtonsoftConverter _sut;

            protected override void Given()
            {
            }

            protected override void When()
            {
                _sut = new NewtonsoftConverter();
            }

            [Fact]
            public void Then_It_Should_Have_Created_A_Valid_Instance()
            {
                _sut.Should().NotBeNull();
            }

            [Fact]
            public void Then_It_Should_Be_An_IJsonConverter()
            {
                _sut.Should().BeAssignableTo<IJsonConverter>();
            }
        }
    }
}