﻿using System;
using DiDrDe.CommonContracts;
using ToolBelt.TestSupport;

namespace DiDrDe.EventStore.Infra.EventStore.UnitTests.TestSupport.Builders
{
    public class IdentityBuilder
    {
        private Guid _value;

        public IdentityBuilder()
        {
            _value = Guid.Empty;
        }

        public IdentityBuilder WithValueSeed(int seed)
        {
            _value = GuidGenerator.Create(seed);
            return this;
        }

        public Identity Build()
        {
            var identity = new Identity(_value);
            return identity;
        }
    }
}