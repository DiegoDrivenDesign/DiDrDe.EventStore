﻿using DiDrDe.EventStore.Infra.EventStore.Contracts.Factories;
using DiDrDe.EventStore.Infra.EventStore.Contracts.Repositories;
using Moq;

namespace DiDrDe.EventStore.Infra.EventStore.UnitTests.TestSupport.Builders
{
    public class EventStoreRetrieverBuilder
    {
        private IDomainEventFactory _domainEventFactory;
        private IEventStoreRepository _eventStoreRepository;

        public EventStoreRetrieverBuilder()
        {
            _domainEventFactory = Mock.Of<IDomainEventFactory>();
            _eventStoreRepository = Mock.Of<IEventStoreRepository>();
        }

        public EventStoreRetrieverBuilder WithDomainEventFactory(IDomainEventFactory domainEventFactory)
        {
            _domainEventFactory = domainEventFactory;
            return this;
        }

        public EventStoreRetrieverBuilder WithEventStoreRepository(IEventStoreRepository eventStoreRepository)
        {
            _eventStoreRepository = eventStoreRepository;
            return this;
        }

        public DomainEventsRetriever Build()
        {
            var eventStoreRetriever = new DomainEventsRetriever(_domainEventFactory, _eventStoreRepository);
            return eventStoreRetriever;
        }
    }
}