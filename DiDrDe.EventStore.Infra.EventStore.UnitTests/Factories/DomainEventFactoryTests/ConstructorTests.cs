﻿using System;
using DiDrDe.EventStore.Infra.EventStore.Contracts;
using DiDrDe.EventStore.Infra.EventStore.Contracts.Converters;
using DiDrDe.EventStore.Infra.EventStore.Contracts.Factories;
using DiDrDe.EventStore.Infra.EventStore.Factories;
using FluentAssertions;
using Moq;
using ToolBelt.TestSupport;
using Xunit;

namespace DiDrDe.EventStore.Infra.EventStore.UnitTests.Factories.DomainEventFactoryTests
{
    public static class ConstructorTests
    {
        public class Given_Valid_Dependencies_When_Constructing_Instance
            : Given_When_Then_Test
        {
            private DomainEventFactory _sut;
            private IBytesConverter _bytesConverter;
            private IJsonConverter _jsonConverter;
            private IMapperResolver _mapperResolver;
            private IEventTypeResolver _typeResolver;

            protected override void Given()
            {
                _bytesConverter = Mock.Of<IBytesConverter>();
                _jsonConverter = Mock.Of<IJsonConverter>();
                _mapperResolver = Mock.Of<IMapperResolver>();
                _typeResolver = Mock.Of<IEventTypeResolver>();
            }

            protected override void When()
            {
                _sut =
                    new DomainEventFactory(
                        _bytesConverter,
                        _jsonConverter,
                        _mapperResolver,
                        _typeResolver);
            }

            [Fact]
            public void Then_It_Should_Have_Created_A_Valid_Instance()
            {
                _sut.Should().NotBeNull();
            }

            [Fact]
            public void Then_It_Should_Be_An_IDomainEventFactory()
            {
                _sut.Should().BeAssignableTo<IDomainEventFactory>();
            }
        }

        public class Given_Null_BytesConverter_Dependency_When_Constructing_Instance
            : Given_When_Then_Test
        {
            private DomainEventFactory _sut;
            private IBytesConverter _bytesConverter;
            private IJsonConverter _jsonConverter;
            private IMapperResolver _mapperResolver;
            private IEventTypeResolver _typeResolver;
            private ArgumentNullException _exception;

            protected override void Given()
            {
                _bytesConverter = null;
                _jsonConverter = Mock.Of<IJsonConverter>();
                _mapperResolver = Mock.Of<IMapperResolver>();
                _typeResolver = Mock.Of<IEventTypeResolver>();
            }

            protected override void When()
            {
                try
                {
                    _sut =
                        new DomainEventFactory(
                            _bytesConverter,
                            _jsonConverter,
                            _mapperResolver,
                            _typeResolver);
                }
                catch (ArgumentNullException exception)
                {
                    _exception = exception;
                }
            }

            [Fact]
            public void Then_It_Should_Be_Null()
            {
                _sut.Should().BeNull();
            }

            [Fact]
            public void Then_It_Should_Throw_An_ArgumentNullException()
            {
                _exception.Should().NotBeNull();
            }
        }

        public class Given_Null_JsonConverter_Dependency_When_Constructing_Instance
            : Given_When_Then_Test
        {
            private DomainEventFactory _sut;
            private IBytesConverter _bytesConverter;
            private IJsonConverter _jsonConverter;
            private IMapperResolver _mapperResolver;
            private IEventTypeResolver _typeResolver;
            private ArgumentNullException _exception;

            protected override void Given()
            {
                _bytesConverter = Mock.Of<IBytesConverter>();
                _jsonConverter = null;
                _mapperResolver = Mock.Of<IMapperResolver>();
                _typeResolver = Mock.Of<IEventTypeResolver>();
            }

            protected override void When()
            {
                try
                {
                    _sut =
                        new DomainEventFactory(
                            _bytesConverter,
                            _jsonConverter,
                            _mapperResolver,
                            _typeResolver);
                }
                catch (ArgumentNullException exception)
                {
                    _exception = exception;
                }
            }

            [Fact]
            public void Then_It_Should_Be_Null()
            {
                _sut.Should().BeNull();
            }

            [Fact]
            public void Then_It_Should_Throw_An_ArgumentNullException()
            {
                _exception.Should().NotBeNull();
            }
        }

        public class Given_Null_MapperResolver_Dependency_When_Constructing_Instance
            : Given_When_Then_Test
        {
            private DomainEventFactory _sut;
            private IBytesConverter _bytesConverter;
            private IJsonConverter _jsonConverter;
            private IMapperResolver _mapperResolver;
            private IEventTypeResolver _typeResolver;
            private ArgumentNullException _exception;

            protected override void Given()
            {
                _bytesConverter = Mock.Of<IBytesConverter>();
                _jsonConverter = Mock.Of<IJsonConverter>();
                _mapperResolver = null;
                _typeResolver = Mock.Of<IEventTypeResolver>();
            }

            protected override void When()
            {
                try
                {
                    _sut =
                        new DomainEventFactory(
                            _bytesConverter,
                            _jsonConverter,
                            _mapperResolver,
                            _typeResolver);
                }
                catch (ArgumentNullException exception)
                {
                    _exception = exception;
                }
            }

            [Fact]
            public void Then_It_Should_Be_Null()
            {
                _sut.Should().BeNull();
            }

            [Fact]
            public void Then_It_Should_Throw_An_ArgumentNullException()
            {
                _exception.Should().NotBeNull();
            }
        }

        public class Given_Null_TypeResolver_Dependency_When_Constructing_Instance
            : Given_When_Then_Test
        {
            private DomainEventFactory _sut;
            private IBytesConverter _bytesConverter;
            private IJsonConverter _jsonConverter;
            private IMapperResolver _mapperResolver;
            private IEventTypeResolver _typeResolver;
            private ArgumentNullException _exception;

            protected override void Given()
            {
                _bytesConverter = Mock.Of<IBytesConverter>();
                _jsonConverter = Mock.Of<IJsonConverter>();
                _mapperResolver = Mock.Of<IMapperResolver>();
                _typeResolver = null;
            }

            protected override void When()
            {
                try
                {
                    _sut =
                        new DomainEventFactory(
                            _bytesConverter,
                            _jsonConverter,
                            _mapperResolver,
                            _typeResolver);
                }
                catch (ArgumentNullException exception)
                {
                    _exception = exception;
                }
            }

            [Fact]
            public void Then_It_Should_Be_Null()
            {
                _sut.Should().BeNull();
            }

            [Fact]
            public void Then_It_Should_Throw_An_ArgumentNullException()
            {
                _exception.Should().NotBeNull();
            }
        }
    }
}