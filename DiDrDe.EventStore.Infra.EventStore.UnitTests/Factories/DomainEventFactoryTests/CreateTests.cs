﻿using System;
using System.Collections.Generic;
using DiDrDe.EventStore.Infra.EventStore.Contracts;
using DiDrDe.EventStore.Infra.EventStore.Contracts.Converters;
using DiDrDe.EventStore.Infra.EventStore.Factories;
using DiDrDe.EventStore.Infra.EventStore.UnitTests.TestSupport.Builders;
using Moq;
using ToolBelt.TestSupport;
using Xunit;

namespace DiDrDe.EventStore.Infra.EventStore.UnitTests.Factories.DomainEventFactoryTests
{
    public static class CreateTests
    {
        public class Given_A_ResolvedEvent_When_Create
            : Given_When_Then_Test
        {
            private DomainEventFactory _sut;
            private Mock<IBytesConverter> _bytesConverterMock;
            private Mock<IJsonConverter> _jsonConverterMock;
            private Mock<IMapperResolver> _mapperResolverMock;
            private IRetrievedEvent _retrievedEvent;
            private Type _domainEventType;
            private byte[] _metadataBytes;
            private byte[] _dataBytes;
            private string _metadataJson;
            private string _dataJson;

            protected override void Given()
            {
                _domainEventType = typeof(object);
                var domainEventTypeName = _domainEventType.Name;
                var serializableEventType = typeof(object);
                var serializableEventTypeName = serializableEventType.Name;
                _metadataBytes = new byte[1024];
                _dataBytes = new byte[2048];
                var metadata =
                    new Dictionary<string, string>
                    {
                        {EventStoreConstants.DomainEventTypeHeader, domainEventTypeName },
                        {EventStoreConstants.SerializableEventTypeHeader, serializableEventTypeName }
                    };

                _metadataJson = "{}";
                _dataJson = "{}";

                _bytesConverterMock = new Mock<IBytesConverter>();
                _bytesConverterMock
                    .Setup(x => x.FromBytes(_metadataBytes))
                    .Returns(_metadataJson);
                _bytesConverterMock
                    .Setup(x => x.FromBytes(_dataBytes))
                    .Returns(_dataJson);
                var bytesConverter = _bytesConverterMock.Object;

                _jsonConverterMock = new Mock<IJsonConverter>();
                _jsonConverterMock
                    .Setup(x => x.Deserialize<Dictionary<string, string>>(_metadataJson))
                    .Returns(metadata);
                var jsonConverter = _jsonConverterMock.Object;

                _mapperResolverMock = new Mock<IMapperResolver>();
                _mapperResolverMock
                    .Setup(x => x.GetDeserializer(It.IsAny<Type>()))
                    .Returns(default(Delegate));
                var mapperResolver = _mapperResolverMock.Object;

                var retrievedEventMock = new Mock<IRetrievedEvent>();
                retrievedEventMock
                    .Setup(x => x.Metadata)
                    .Returns(_metadataBytes);

                retrievedEventMock
                    .Setup(x => x.Data)
                    .Returns(_dataBytes);

                _retrievedEvent = retrievedEventMock.Object;

                _sut =
                    new DomainEventFactoryBuilder()
                        .WithBytesConverter(bytesConverter)
                        .WithJsonConverter(jsonConverter)
                        .WithMapperResolver(mapperResolver)
                        .Build();
            }

            protected override void When()
            {
                _sut.Create(_retrievedEvent);
            }

            [Fact]
            public void Then_It_Should_Get_The_Json_From_Event_Metadata_Bytes()
            {
                _bytesConverterMock.Verify(x => x.FromBytes(_metadataBytes), Times.Once);
            }

            [Fact]
            public void Then_It_Should_Get_The_Json_From_Event_Data()
            {
                _bytesConverterMock.Verify(x => x.FromBytes(_dataBytes), Times.Once);
            }

            [Fact]
            public void Then_It_Should_Deserialize_Event_Metadata()
            {
                _jsonConverterMock.Verify(x => x.Deserialize<Dictionary<string, string>>(_metadataJson), Times.Once);
            }

            [Fact]
            public void Then_It_Should_Deserialize_Event_Data()
            {
                _jsonConverterMock.Verify(x => x.Deserialize<Dictionary<string, string>>(_dataJson), Times.Once);
            }

            [Fact]
            public void Then_It_Should_Retrieve_Mapper_From_MapperResolver_By_Its_Domain_Event_Type()
            {
                _mapperResolverMock.Verify(x => x.GetDeserializer(It.IsAny<Type>()), Times.Once);
            }
        }
    }
}