﻿using System;
using DiDrDe.EventStore.Infra.EventStore.Contracts;
using DiDrDe.EventStore.Infra.EventStore.Contracts.Converters;
using DiDrDe.EventStore.Infra.EventStore.Contracts.Factories;
using DiDrDe.EventStore.Infra.EventStore.Factories;
using FluentAssertions;
using Moq;
using ToolBelt.TestSupport;
using Xunit;

namespace DiDrDe.EventStore.Infra.EventStore.UnitTests.Factories.EventDataFactoryTests
{
    public static class ConstructorTests
    {
        public class Given_Valid_Dependencies_When_Constructing_Instance
            : Given_When_Then_Test
        {
            private EventDataFactory _sut;
            private IIdFactory _idFactory;
            private IJsonConverter _jsonConverter;
            private IBytesConverter _bytesConverter;
            private IMapperResolver _mapperResolver;

            protected override void Given()
            {
                _idFactory = Mock.Of<IIdFactory>();
                _jsonConverter = Mock.Of<IJsonConverter>();
                _bytesConverter = Mock.Of<IBytesConverter>();
                _mapperResolver = Mock.Of<IMapperResolver>();
            }

            protected override void When()
            {
                _sut = new EventDataFactory(_idFactory, _jsonConverter, _bytesConverter, _mapperResolver);
            }

            [Fact]
            public void Then_It_Should_Have_Created_A_Valid_Instance()
            {
                _sut.Should().NotBeNull();
            }

            [Fact]
            public void Then_It_Should_Be_An_IEventDataFactory()
            {
                _sut.Should().BeAssignableTo<IEventDataFactory>();
            }
        }

        public class Given_Null_IdFactory_Dependency_When_Constructing_Instance
            : Given_When_Then_Test
        {
            private EventDataFactory _sut;
            private IIdFactory _idFactory;
            private IJsonConverter _jsonConverter;
            private IBytesConverter _bytesConverter;
            private IMapperResolver _mapperResolver;
            private ArgumentNullException _exception;

            protected override void Given()
            {
                _idFactory = null;
                _jsonConverter = Mock.Of<IJsonConverter>();
                _bytesConverter = Mock.Of<IBytesConverter>();
                _mapperResolver = Mock.Of<IMapperResolver>();
            }

            protected override void When()
            {
                try
                {
                    _sut = new EventDataFactory(_idFactory, _jsonConverter, _bytesConverter, _mapperResolver);
                }
                catch (ArgumentNullException exception)
                {
                    _exception = exception;
                }
            }

            [Fact]
            public void Then_It_Should_Be_Null()
            {
                _sut.Should().BeNull();
            }

            [Fact]
            public void Then_It_Should_Throw_An_ArgumentNullException()
            {
                _exception.Should().NotBeNull();
            }
        }

        public class Given_Null_JsonConverter_Dependency_When_Constructing_Instance
            : Given_When_Then_Test
        {
            private EventDataFactory _sut;
            private IIdFactory _idFactory;
            private IJsonConverter _jsonConverter;
            private IBytesConverter _bytesConverter;
            private IMapperResolver _mapperResolver;
            private ArgumentNullException _exception;

            protected override void Given()
            {
                _idFactory = Mock.Of<IIdFactory>();
                _jsonConverter = null;
                _bytesConverter = Mock.Of<IBytesConverter>();
                _mapperResolver = Mock.Of<IMapperResolver>();
            }

            protected override void When()
            {
                try
                {
                    _sut = new EventDataFactory(_idFactory, _jsonConverter, _bytesConverter, _mapperResolver);
                }
                catch (ArgumentNullException exception)
                {
                    _exception = exception;
                }
            }

            [Fact]
            public void Then_It_Should_Be_Null()
            {
                _sut.Should().BeNull();
            }

            [Fact]
            public void Then_It_Should_Throw_An_ArgumentNullException()
            {
                _exception.Should().NotBeNull();
            }
        }

        public class Given_Null_BytesConverter_Dependency_When_Constructing_Instance
            : Given_When_Then_Test
        {
            private EventDataFactory _sut;
            private IIdFactory _idFactory;
            private IJsonConverter _jsonConverter;
            private IBytesConverter _bytesConverter;
            private IMapperResolver _mapperResolver;
            private ArgumentNullException _exception;

            protected override void Given()
            {
                _idFactory = Mock.Of<IIdFactory>();
                _jsonConverter = Mock.Of<IJsonConverter>();
                _bytesConverter = null;
                _mapperResolver = Mock.Of<IMapperResolver>();
            }

            protected override void When()
            {
                try
                {
                    _sut = new EventDataFactory(_idFactory, _jsonConverter, _bytesConverter, _mapperResolver);
                }
                catch (ArgumentNullException exception)
                {
                    _exception = exception;
                }
            }

            [Fact]
            public void Then_It_Should_Be_Null()
            {
                _sut.Should().BeNull();
            }

            [Fact]
            public void Then_It_Should_Throw_An_ArgumentNullException()
            {
                _exception.Should().NotBeNull();
            }
        }

        public class Given_Null_MapperResolver_Dependency_When_Constructing_Instance
            : Given_When_Then_Test
        {
            private EventDataFactory _sut;
            private IIdFactory _idFactory;
            private IJsonConverter _jsonConverter;
            private IBytesConverter _bytesConverter;
            private IMapperResolver _mapperResolver;
            private ArgumentNullException _exception;

            protected override void Given()
            {
                _idFactory = Mock.Of<IIdFactory>();
                _jsonConverter = Mock.Of<IJsonConverter>();
                _bytesConverter = Mock.Of<IBytesConverter>();
                _mapperResolver = null;
            }

            protected override void When()
            {
                try
                {
                    _sut = new EventDataFactory(_idFactory, _jsonConverter, _bytesConverter, _mapperResolver);
                }
                catch (ArgumentNullException exception)
                {
                    _exception = exception;
                }
            }

            [Fact]
            public void Then_It_Should_Be_Null()
            {
                _sut.Should().BeNull();
            }

            [Fact]
            public void Then_It_Should_Throw_An_ArgumentNullException()
            {
                _exception.Should().NotBeNull();
            }
        }

        public class Given_Null_Dependencies_When_Constructing_Instance
            : Given_When_Then_Test
        {
            private EventDataFactory _sut;
            private IIdFactory _idFactory;
            private IJsonConverter _jsonConverter;
            private IBytesConverter _bytesConverter;
            private IMapperResolver _mapperResolver;
            private ArgumentNullException _exception;

            protected override void Given()
            {
                _idFactory = null;
                _jsonConverter = null;
                _bytesConverter = null;
                _mapperResolver = null;
            }

            protected override void When()
            {
                try
                {
                    _sut = new EventDataFactory(_idFactory, _jsonConverter, _bytesConverter, _mapperResolver);
                }
                catch (ArgumentNullException exception)
                {
                    _exception = exception;
                }
            }

            [Fact]
            public void Then_It_Should_Be_Null()
            {
                _sut.Should().BeNull();
            }

            [Fact]
            public void Then_It_Should_Throw_An_ArgumentNullException()
            {
                _exception.Should().NotBeNull();
            }
        }
    }
}