﻿using System;
using System.Collections.Generic;
using System.Linq;
using DiDrDe.EventStore.Infra.EventStore.Exceptions;
using DiDrDe.EventStore.Infra.EventStore.Factories;
using DiDrDe.EventStore.Infra.EventStore.IoCC;
using DiDrDe.EventStore.Infra.EventStore.UnitTests.TestSupport;
using FluentAssertions;
using ToolBelt.TestSupport;
using Xunit;

namespace DiDrDe.EventStore.Infra.EventStore.UnitTests.Factories.EventTypeResolverFactoryTests
{
    public static class CreateTests
    {
        public class Given_A_Collection_Of_Unique_Event_Types_And_A_Null_Dictionary_Of_Custom_Event_Mappers_When_Create
            : Given_When_Then_Test
        {
            private IEnumerable<Type> _eventTypes;
            private IReadOnlyDictionary<Type, DomainEventSerializerDeserializer> _domainEventSerializerDeserializers;
            private EventTypeResolver _result;
            private Exception _exception;
            private EventTypeResolver _expectedTypeResolver;

            protected override void Given()
            {
                var fakeDomainEventType = typeof(FakeDomainEvent);
                var fakeSerializableEvent = typeof(FakeSerializableEvent);
                _eventTypes =
                    new List<Type>
                    {
                        fakeDomainEventType,
                        fakeSerializableEvent
                    };
                _domainEventSerializerDeserializers = null;
                var expectedEventTypes =
                    new List<Type>
                    {
                        fakeDomainEventType,
                        fakeSerializableEvent
                    };
                _expectedTypeResolver = new EventTypeResolver(expectedEventTypes);
            }

            protected override void When()
            {
                try
                {
                    _result =
                        EventTypeResolverFactory.Create(_eventTypes, _domainEventSerializerDeserializers);
                }
                catch (Exception exception)
                {
                    _exception = exception;
                }
            }

            [Fact]
            public void Then_It_Should_Execute_Without_Exceptions()
            {
                _exception.Should().BeNull();
            }


            [Fact]
            public void Then_It_Should_Return_The_Expected_Type_Resolver()
            {
                _result.Should().BeEquivalentTo(_expectedTypeResolver);
            }
        }

        public class Given_A_Collection_Of_Non_Unique_Event_Types_And_A_Null_Dictionary_Of_Custom_Event_Mappers_When_Create
            : Given_When_Then_Test
        {
            private IEnumerable<Type> _eventTypes;
            private IReadOnlyDictionary<Type, DomainEventSerializerDeserializer> _domainEventSerializerDeserializers;
            private EventTypeResolver _result;
            private Exception _exception;
            private EventTypeResolver _expectedTypeResolver;

            protected override void Given()
            {
                var fakeDomainEventType = typeof(FakeDomainEvent);
                var fakeSerializableEvent = typeof(FakeSerializableEvent);
                _eventTypes =
                    new List<Type>
                    {
                        fakeDomainEventType,
                        fakeSerializableEvent,
                        fakeDomainEventType
                    };
                _domainEventSerializerDeserializers = null;
                var expectedEventTypes =
                    new List<Type>
                    {
                        fakeDomainEventType,
                        fakeSerializableEvent
                    };
                _expectedTypeResolver = new EventTypeResolver(expectedEventTypes);
            }

            protected override void When()
            {
                try
                {
                    _result =
                        EventTypeResolverFactory.Create(_eventTypes, _domainEventSerializerDeserializers);
                }
                catch (Exception exception)
                {
                    _exception = exception;
                }
            }

            [Fact]
            public void Then_It_Should_Execute_Without_Exceptions()
            {
                _exception.Should().BeNull();
            }


            [Fact]
            public void Then_It_Should_Return_The_Expected_Type_Resolver()
            {
                _result.Should().BeEquivalentTo(_expectedTypeResolver);
            }
        }

        public class Given_A_Collection_With_Duplicate_Event_Class_Names_But_Different_Types_And_A_Null_Dictionary_Of_Custom_Event_Mappers_When_Create
            : Given_When_Then_Test
        {
            private IEnumerable<Type> _eventTypes;
            private IReadOnlyDictionary<Type, DomainEventSerializerDeserializer> _domainEventSerializerDeserializers;
            private EventTypeResolver _result;
            private DuplicateEventNameException _exception;
            private string _expectedDuplicateClassName;

            protected override void Given()
            {
                var fakeDomainEventType = typeof(FakeDomainEvent);
                var fakeSerializableEvent = typeof(FakeSerializableEvent);
                var fakeDomainEventTypeDifferentNamespace = typeof(TestSupport.EventsWithSameNames.FakeDomainEvent);
                _eventTypes =
                    new List<Type>
                    {
                        fakeDomainEventType,
                        fakeSerializableEvent,
                        fakeDomainEventTypeDifferentNamespace
                    };
                _domainEventSerializerDeserializers = null;

                _expectedDuplicateClassName = nameof(FakeDomainEvent);
            }

            protected override void When()
            {
                try
                {
                    _result =
                        EventTypeResolverFactory.Create(_eventTypes, _domainEventSerializerDeserializers);
                }
                catch (DuplicateEventNameException exception)
                {
                    _exception = exception;
                }
            }

            [Fact]
            public void Then_It_Should_Throw_A_DuplicateEventNameException()
            {
                _exception.Should().NotBeNull();
            }

            [Fact]
            public void Then_It_Should_Contain_The_Name_Of_The_Duplicate_Class_In_The_Message()
            {
                _exception.Message.Should().Contain(_expectedDuplicateClassName);
            }
        }

        public class Given_A_Collection_Of_Empty_Event_Types_And_A_Null_Dictionary_Of_Custom_Event_Mappers_When_Create
            : Given_When_Then_Test
        {
            private IEnumerable<Type> _eventTypes;
            private IReadOnlyDictionary<Type, DomainEventSerializerDeserializer> _domainEventSerializerDeserializers;
            private EventTypeResolver _result;
            private Exception _exception;
            private EventTypeResolver _expectedTypeResolver;

            protected override void Given()
            {
                _eventTypes = Enumerable.Empty<Type>();
                _domainEventSerializerDeserializers = null;
                var expectedEventTypes = Enumerable.Empty<Type>();
                _expectedTypeResolver = new EventTypeResolver(expectedEventTypes);
            }

            protected override void When()
            {
                try
                {
                    _result =
                        EventTypeResolverFactory.Create(_eventTypes, _domainEventSerializerDeserializers);
                }
                catch (Exception exception)
                {
                    _exception = exception;
                }
            }

            [Fact]
            public void Then_It_Should_Execute_Without_Exceptions()
            {
                _exception.Should().BeNull();
            }

            [Fact]
            public void Then_It_Should_Return_The_Expected_Type_Resolver()
            {
                _result.Should().BeEquivalentTo(_expectedTypeResolver);
            }
        }

        public class Given_A_Collection_Of_Null_Event_Types_When_Create
            : Given_When_Then_Test
        {
            private IEnumerable<Type> _eventTypes;
            private IReadOnlyDictionary<Type, DomainEventSerializerDeserializer> _domainEventSerializerDeserializers;
            private EventTypeResolver _result;
            private ArgumentNullException _exception;

            protected override void Given()
            {
                _eventTypes = null;
                _domainEventSerializerDeserializers = null;
            }

            protected override void When()
            {
                try
                {
                    _result =
                        EventTypeResolverFactory.Create(_eventTypes, _domainEventSerializerDeserializers);
                }
                catch (ArgumentNullException exception)
                {
                    _exception = exception;
                }
            }

            [Fact]
            public void Then_It_Should_Throw_A_ArgumentNullException()
            {
                _exception.Should().NotBeNull();
            }
        }
    }
}