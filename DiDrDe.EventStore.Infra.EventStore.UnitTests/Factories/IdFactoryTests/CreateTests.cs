﻿using System;
using DiDrDe.EventStore.Infra.EventStore.Factories;
using FluentAssertions;
using ToolBelt.TestSupport;
using Xunit;

namespace DiDrDe.EventStore.Infra.EventStore.UnitTests.Factories.IdFactoryTests
{
    public static class CreateTests
    {
        public class Given_An_IdFactory_When_Create
            : Given_When_Then_Test
        {
            private IdFactory _sut;
            private Guid _result;

            protected override void Given()
            {
                _sut = new IdFactory();
            }

            protected override void When()
            {
                _result = _sut.Create();
            }

            [Fact]
            public void Then_It_Should_Not_Be_Empty_Guid()
            {
                _result.Should().NotBe(Guid.Empty);
            }
        }
    }
}