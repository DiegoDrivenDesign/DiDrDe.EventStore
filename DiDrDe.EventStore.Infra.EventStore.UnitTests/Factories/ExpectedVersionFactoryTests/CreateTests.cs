﻿using DiDrDe.EventStore.Infra.EventStore.Factories;
using EventStore.ClientAPI;
using FluentAssertions;
using ToolBelt.TestSupport;
using Xunit;

namespace DiDrDe.EventStore.Infra.EventStore.UnitTests.Factories.ExpectedVersionFactoryTests
{
    public static class CreateTests
    {
        public class Given_An_Aggregate_Root_Version_Zero_And_A_Count_Of_Events_Of_Zero_When_Create
            : Given_When_Then_Test
        {
            private ExpectedVersionFactory _sut;
            private int _aggregateRootVersion;
            private int _numberOfEvents;
            private long _expectedResult;
            private long _result;

            protected override void Given()
            {
                _aggregateRootVersion = 0;
                _numberOfEvents = 0;
                _expectedResult = ExpectedVersion.EmptyStream;
                _sut = new ExpectedVersionFactory();
            }

            protected override void When()
            {
                _result = _sut.Create(_aggregateRootVersion, _numberOfEvents);
            }

            [Fact]
            public void Then_It_Should_Be_An_Empty_Stream_Result()
            {
                _result.Should().Be(_expectedResult);
            }
        }

        public class Given_An_Aggregate_Root_Version_Zero_And_A_Count_Of_Events_Of_One_When_Create
            : Given_When_Then_Test
        {
            private ExpectedVersionFactory _sut;
            private int _aggregateRootVersion;
            private int _numberOfEvents;
            private long _expectedResult;
            private long _result;

            protected override void Given()
            {
                _aggregateRootVersion = 0;
                _numberOfEvents = 1;
                _expectedResult = ExpectedVersion.Any;
                _sut = new ExpectedVersionFactory();
            }

            protected override void When()
            {
                _result = _sut.Create(_aggregateRootVersion, _numberOfEvents);
            }

            [Fact]
            public void Then_It_Should_Be_Any_Stream_Result()
            {
                _result.Should().Be(_expectedResult);
            }
        }

        public class Given_An_Aggregate_Root_Version_One_And_A_Count_Of_Events_Of_Zero_When_Create
            : Given_When_Then_Test
        {
            private ExpectedVersionFactory _sut;
            private int _aggregateRootVersion;
            private int _numberOfEvents;
            private long _expectedResult;
            private long _result;

            protected override void Given()
            {
                _aggregateRootVersion = 1;
                _numberOfEvents = 0;
                _expectedResult = 0;
                _sut = new ExpectedVersionFactory();
            }

            protected override void When()
            {
                _result = _sut.Create(_aggregateRootVersion, _numberOfEvents);
            }

            [Fact]
            public void Then_It_Should_Be_Any_Stream_Result()
            {
                _result.Should().Be(_expectedResult);
            }
        }

        public class Given_An_Aggregate_Root_Version_Two_And_A_Count_Of_Events_Of_One_When_Create
            : Given_When_Then_Test
        {
            private ExpectedVersionFactory _sut;
            private int _aggregateRootVersion;
            private int _numberOfEvents;
            private long _expectedResult;
            private long _result;

            protected override void Given()
            {
                _aggregateRootVersion = 2;
                _numberOfEvents = 1;
                _expectedResult = 0;
                _sut = new ExpectedVersionFactory();
            }

            protected override void When()
            {
                _result = _sut.Create(_aggregateRootVersion, _numberOfEvents);
            }

            [Fact]
            public void Then_It_Should_Be_Any_Stream_Result()
            {
                _result.Should().Be(_expectedResult);
            }
        }
    }
}