﻿using System;
using DiDrDe.EventStore.Infra.EventStore.Contracts.Configuration;
using DiDrDe.EventStore.Infra.EventStore.Contracts.Factories;
using DiDrDe.EventStore.Infra.EventStore.Contracts.Repositories;
using DiDrDe.EventStore.Infra.EventStore.Repositories;
using EventStore.ClientAPI;
using FluentAssertions;
using Moq;
using ToolBelt.TestSupport;
using Xunit;

namespace DiDrDe.EventStore.Infra.EventStore.UnitTests.Repositories.EventStoreRepositoryTests
{
    public static class ConstructorTests
    {
        public class Given_Valid_Dependencies_When_Constructing_Instance
            : Given_When_Then_Test
        {
            private EventStoreRepository _sut;
            private IEventStoreConnection _eventStoreConnection;
            private IRetrievedEventFactory _retrievedEventFactory;
            private ITimeoutConfiguration _timeoutConfiguration;

            protected override void Given()
            {
                _eventStoreConnection = Mock.Of<IEventStoreConnection>();
                _retrievedEventFactory = Mock.Of<IRetrievedEventFactory>();
                _timeoutConfiguration = Mock.Of<ITimeoutConfiguration>();
            }

            protected override void When()
            {
                _sut = new EventStoreRepository(_eventStoreConnection, _retrievedEventFactory, _timeoutConfiguration);
            }

            [Fact]
            public void Then_It_Should_Have_Created_A_Valid_Instance()
            {
                _sut.Should().NotBeNull();
            }

            [Fact]
            public void Then_It_Should_Be_An_IEventStoreRepository()
            {
                _sut.Should().BeAssignableTo<IEventStoreRepository>();
            }
        }

        public class Given_Null_EventStoreConnection_When_Constructing_Instance
            : Given_When_Then_Test
        {
            private EventStoreRepository _sut;
            private IEventStoreConnection _eventStoreConnection;
            private IRetrievedEventFactory _retrievedEventFactory;
            private ArgumentNullException _exception;
            private ITimeoutConfiguration _timeoutConfiguration;

            protected override void Given()
            {
                _eventStoreConnection = null;
                _retrievedEventFactory = Mock.Of<IRetrievedEventFactory>();
                _timeoutConfiguration = Mock.Of<ITimeoutConfiguration>();
            }

            protected override void When()
            {
                try
                {
                    _sut = new EventStoreRepository(_eventStoreConnection, _retrievedEventFactory, _timeoutConfiguration);
                }
                catch (ArgumentNullException exception)
                {
                    _exception = exception;
                }
            }

            [Fact]
            public void Then_It_Should_Be_Null()
            {
                _sut.Should().BeNull();
            }

            [Fact]
            public void Then_It_Should_Throw_An_ArgumentNullException()
            {
                _exception.Should().NotBeNull();
            }
        }

        public class Given_Null_RetrievedEventFactory_When_Constructing_Instance
            : Given_When_Then_Test
        {
            private EventStoreRepository _sut;
            private IEventStoreConnection _eventStoreConnection;
            private IRetrievedEventFactory _retrievedEventFactory;
            private ArgumentNullException _exception;
            private ITimeoutConfiguration _timeoutConfiguration;

            protected override void Given()
            {
                _eventStoreConnection = Mock.Of<IEventStoreConnection>();
                _retrievedEventFactory = null;
                _timeoutConfiguration = Mock.Of<ITimeoutConfiguration>();
            }

            protected override void When()
            {
                try
                {
                    _sut = new EventStoreRepository(_eventStoreConnection, _retrievedEventFactory, _timeoutConfiguration);
                }
                catch (ArgumentNullException exception)
                {
                    _exception = exception;
                }
            }

            [Fact]
            public void Then_It_Should_Be_Null()
            {
                _sut.Should().BeNull();
            }

            [Fact]
            public void Then_It_Should_Throw_An_ArgumentNullException()
            {
                _exception.Should().NotBeNull();
            }
        }

        public class Given_Null_TimeoutConfiguration_When_Constructing_Instance
            : Given_When_Then_Test
        {
            private EventStoreRepository _sut;
            private IEventStoreConnection _eventStoreConnection;
            private IRetrievedEventFactory _retrievedEventFactory;
            private ITimeoutConfiguration _timeoutConfiguration;
            private ArgumentNullException _exception;

            protected override void Given()
            {
                _eventStoreConnection = Mock.Of<IEventStoreConnection>();
                _retrievedEventFactory = Mock.Of<IRetrievedEventFactory>();
                _timeoutConfiguration = null;
            }

            protected override void When()
            {
                try
                {
                    _sut = new EventStoreRepository(_eventStoreConnection, _retrievedEventFactory, _timeoutConfiguration);
                }
                catch (ArgumentNullException exception)
                {
                    _exception = exception;
                }
            }

            [Fact]
            public void Then_It_Should_Be_Null()
            {
                _sut.Should().BeNull();
            }

            [Fact]
            public void Then_It_Should_Throw_An_ArgumentNullException()
            {
                _exception.Should().NotBeNull();
            }
        }
    }
}