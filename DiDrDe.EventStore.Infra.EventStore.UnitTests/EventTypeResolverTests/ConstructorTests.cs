﻿using System;
using System.Collections.Generic;
using DiDrDe.EventStore.Infra.EventStore.Contracts;
using FluentAssertions;
using Moq;
using ToolBelt.TestSupport;
using Xunit;

namespace DiDrDe.EventStore.Infra.EventStore.UnitTests.EventTypeResolverTests
{
    public static class ConstructorTests
    {
        public class Given_Valid_Dependencies_When_Constructing_Instance
            : Given_When_Then_Test
        {
            private EventTypeResolver _sut;
            private IEnumerable<Type> _eventTypes;

            protected override void Given()
            {
                _eventTypes = Mock.Of<IEnumerable<Type>>();
            }

            protected override void When()
            {
                _sut = new EventTypeResolver(_eventTypes);
            }

            [Fact]
            public void Then_It_Should_Have_Created_A_Valid_Instance()
            {
                _sut.Should().NotBeNull();
            }

            [Fact]
            public void Then_It_Should_Be_An_ITypeResolver()
            {
                _sut.Should().BeAssignableTo<IEventTypeResolver>();
            }
        }
    }
}