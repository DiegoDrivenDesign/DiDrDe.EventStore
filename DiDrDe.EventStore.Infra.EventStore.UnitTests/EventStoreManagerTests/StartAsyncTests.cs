﻿using System.Threading.Tasks;
using EventStore.ClientAPI;
using Moq;
using ToolBelt.TestSupport;
using Xunit;

namespace DiDrDe.EventStore.Infra.EventStore.UnitTests.EventStoreManagerTests
{
    public static class StartAsyncTests
    {
        public class Given_An_Event_Store_Connection_When_StartAsync
            : Given_WhenAsync_Then_Test
        {
            private EventStoreManager _sut;
            private Mock<IEventStoreConnection> _eventStoreConnectionMock;

            protected override void Given()
            {
                _eventStoreConnectionMock = new Mock<IEventStoreConnection>();
                var eventStoreConnection = _eventStoreConnectionMock.Object;
                _sut = new EventStoreManager(eventStoreConnection);
            }

            protected override async Task WhenAsync()
            {
                await _sut.StartAsync();
            }

            [Fact]
            public void Then_It_Should_Start_The_Connection()
            {
                _eventStoreConnectionMock.Verify(x => x.ConnectAsync(), Times.Once);
            }
        }
    }
}