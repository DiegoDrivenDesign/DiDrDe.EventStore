﻿using System;
using DiDrDe.EventStore.Infra.EventStore.Exceptions;
using FluentAssertions;
using Moq;
using ToolBelt.TestSupport;
using Xunit;

namespace DiDrDe.EventStore.Infra.EventStore.UnitTests.Exceptions.CouldNotPersistEventsExceptionTests
{
    public static class ConstructorTests
    {
        public class Given_Valid_Dependencies_When_Constructing_Instance
            : Given_When_Then_Test
        {
            private CouldNotPersistEventsException _sut;
            private string _message;
            private Exception _innerException;

            protected override void Given()
            {
                _message = "foo";
                _innerException = Mock.Of<Exception>();
            }

            protected override void When()
            {
                _sut = new CouldNotPersistEventsException(_message, _innerException);
            }

            [Fact]
            public void Then_It_Should_Have_Created_A_Valid_Instance()
            {
                _sut.Should().NotBeNull();
            }

            [Fact]
            public void Then_It_Should_Be_An_Exception()
            {
                _sut.Should().BeAssignableTo<Exception>();
            }
        }
    }
}