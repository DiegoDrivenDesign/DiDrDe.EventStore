﻿using System;

namespace DiDrDe.EventStore.Sample.Common
{
    public static class Constants
    {
        public static class EventStoreSettings
        {
            public const string ConnectionString = "ConnectTo=tcp://admin:changeit@127.0.0.1:1113; HeartBeatTimeout=500;";
            //public const string ConnectionString = "ConnectTo=discover://admin:changeit@127.0.0.1:2114; HeartBeatTimeout=500";
            public const int TimeoutSeconds = 5;
        }

        public static class TestConfiguration
        {
            public static Guid AggregateId = new Guid("00000000-0000-0000-0000-000000000001");
            public const int NumberOfEvents = 50;
            public const int GapMilliseconds = 5000;
        }
    }
}
