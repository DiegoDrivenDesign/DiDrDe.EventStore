﻿using DiDrDe.CommonContracts;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DiDrDe.EventStore
{
    public interface IEventStore
    {
        Task<IEnumerable<IDomainEvent>> GetEvents(Guid aggregateId, Type aggregateType);
        Task Persist(IAggregateRoot aggregateRoot, IEnumerable<IDomainEvent> events);
    }
}