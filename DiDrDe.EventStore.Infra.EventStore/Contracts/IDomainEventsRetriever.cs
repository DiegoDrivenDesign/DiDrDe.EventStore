﻿using System.Collections.Generic;
using System.Threading.Tasks;
using DiDrDe.CommonContracts;

namespace DiDrDe.EventStore.Infra.EventStore.Contracts
{
    public interface IDomainEventsRetriever
    {
        Task<IEnumerable<IDomainEvent>> GetDomainEventsAsync(
            string streamName,
            int start = 0,
            int count = 4096,
            bool resolveLinksTo = false);
    }
}