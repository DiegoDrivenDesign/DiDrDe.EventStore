﻿namespace DiDrDe.EventStore.Infra.EventStore.Contracts
{
    public interface IRetrievedEvent
    {
        byte[] Data { get; }
        byte[] Metadata { get; }
    }
}