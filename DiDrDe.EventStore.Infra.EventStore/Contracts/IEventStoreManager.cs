﻿using System.Threading.Tasks;

namespace DiDrDe.EventStore.Infra.EventStore.Contracts
{
    public interface IEventStoreManager
    {
        Task StartAsync();
        void Start();
        void Stop();
    }
}