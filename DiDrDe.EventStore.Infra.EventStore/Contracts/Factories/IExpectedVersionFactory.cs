﻿namespace DiDrDe.EventStore.Infra.EventStore.Contracts.Factories
{
    public interface IExpectedVersionFactory
    {
        long Create(int aggregateRootVersion, int numberOfEvents);
    }
}