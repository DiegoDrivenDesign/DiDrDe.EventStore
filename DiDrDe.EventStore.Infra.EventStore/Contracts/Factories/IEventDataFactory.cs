﻿using DiDrDe.CommonContracts;
using EventStore.ClientAPI;

namespace DiDrDe.EventStore.Infra.EventStore.Contracts.Factories
{
    public interface IEventDataFactory
    {
        EventData Create(IDomainEvent domainEvent, IAggregateRoot aggregateRootType);
    }
}