﻿using EventStore.ClientAPI;

namespace DiDrDe.EventStore.Infra.EventStore.Contracts.Factories
{
    public interface IRetrievedEventFactory
    {
        RetrievedEvent Create(ResolvedEvent resolvedEvent);
    }
}