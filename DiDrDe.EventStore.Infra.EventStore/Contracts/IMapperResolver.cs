﻿using System;

namespace DiDrDe.EventStore.Infra.EventStore.Contracts
{
    public interface IMapperResolver
    {
        Delegate GetSerializer(Type originType);
        Delegate GetDeserializer(Type destinationType);
    }
}