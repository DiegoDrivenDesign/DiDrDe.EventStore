﻿using DiDrDe.CommonContracts;

namespace DiDrDe.EventStore.Infra.EventStore.Contracts
{
    public interface IEventStoreReadiness
        : IReadiness
    {
    }
}