﻿namespace DiDrDe.EventStore.Infra.EventStore.Contracts.Converters
{
    public interface IBytesConverter
    {
        byte[] ToBytes(string source);
        string FromBytes(byte[] bytes);
    }
}