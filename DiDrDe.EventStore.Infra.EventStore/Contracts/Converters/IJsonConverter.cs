﻿using System;

namespace DiDrDe.EventStore.Infra.EventStore.Contracts.Converters
{
    public interface IJsonConverter
    {
        string Serialize(object source);
        T Deserialize<T>(string json);
        object Deserialize(string json, Type type);
    }
}