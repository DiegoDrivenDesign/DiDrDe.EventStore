﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DiDrDe.EventStore.Infra.EventStore.Contracts.Configuration;
using DiDrDe.EventStore.Infra.EventStore.Contracts.Factories;
using DiDrDe.EventStore.Infra.EventStore.Contracts.Repositories;
using DiDrDe.EventStore.Infra.EventStore.Extensions;
using EventStore.ClientAPI;

namespace DiDrDe.EventStore.Infra.EventStore.Repositories
{
    public class EventStoreRepository
        : IEventStoreRepository
    {
        private readonly IEventStoreConnection _eventStoreConnection;
        private readonly IRetrievedEventFactory _retrievedEventFactory;
        private readonly ITimeoutConfiguration _timeoutConfiguration;

        public EventStoreRepository(
            IEventStoreConnection eventStoreConnection,
            IRetrievedEventFactory retrievedEventFactory, 
            ITimeoutConfiguration timeoutConfiguration)
        {
            _eventStoreConnection = eventStoreConnection ?? throw new ArgumentNullException(nameof(eventStoreConnection));
            _retrievedEventFactory = retrievedEventFactory ?? throw new ArgumentNullException(nameof(retrievedEventFactory));
            _timeoutConfiguration = timeoutConfiguration ?? throw new ArgumentNullException(nameof(timeoutConfiguration));
        }

        public async Task Save(string streamName, long expectedVersion, IEnumerable<EventData> events)
        {
            var timeout = _timeoutConfiguration.TimeSpan;
            var saveTask =
                _eventStoreConnection.AppendToStreamAsync(streamName, expectedVersion, events)
                    .TimeoutAfter(timeout);
            await saveTask;
        }

        public async Task<IEnumerable<Contracts.IRetrievedEvent>> Get(string streamName, int start, int count, bool resolveLinksTo)
        {
            var timeout = _timeoutConfiguration.TimeSpan;
            var readTask =
                _eventStoreConnection.ReadStreamEventsForwardAsync(streamName, start, count, resolveLinksTo)
                    .TimeoutAfter(timeout);
            var streamEventsSlice = await readTask;
            var events = streamEventsSlice.Events;
            var retrievedEvents =
                events
                    .Select(x => _retrievedEventFactory.Create(x))
                    .ToList();
            return retrievedEvents;
        }
    }
}