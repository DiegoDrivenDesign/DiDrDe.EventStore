﻿using System.Threading.Tasks;
using DiDrDe.EventStore.Infra.EventStore.Contracts;
using EventStore.ClientAPI;

namespace DiDrDe.EventStore.Infra.EventStore
{
    public class EventStoreManager
        : IEventStoreManager
    {
        private readonly IEventStoreConnection _eventStoreConnection;

        public EventStoreManager(IEventStoreConnection eventStoreConnection)
        {
            _eventStoreConnection = eventStoreConnection;
        }

        public async Task StartAsync()
        {
            await _eventStoreConnection.ConnectAsync();
        }

        public void Start()
        {
            _eventStoreConnection.ConnectAsync().GetAwaiter().GetResult();
        }

        public void Stop()
        {
            _eventStoreConnection.Close();
        }
    }
}