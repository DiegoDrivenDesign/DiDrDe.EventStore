﻿using System;
using DiDrDe.EventStore.Infra.EventStore.Contracts.Converters;
using Newtonsoft.Json;

namespace DiDrDe.EventStore.Infra.EventStore.Converters
{
    public class NewtonsoftConverter
        : IJsonConverter
    {
        public string Serialize(object source)
        {
            var serializedObject = JsonConvert.SerializeObject(source);
            return serializedObject;
        }

        public T Deserialize<T>(string json)
        {
            var destinationType = typeof(T);
            var deserializedObject = Deserialize(json, destinationType);
            return (T)deserializedObject;
        }

        public object Deserialize(string json, Type type)
        {
            var deserializedObject = JsonConvert.DeserializeObject(json, type);
            return deserializedObject;
        }
    }
}