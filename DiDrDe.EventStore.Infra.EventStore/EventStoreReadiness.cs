﻿using System;
using System.Threading.Tasks;
using DiDrDe.EventStore.Infra.EventStore.Contracts;
using EventStore.ClientAPI;

namespace DiDrDe.EventStore.Infra.EventStore
{
    public class EventStoreReadiness
        : IEventStoreReadiness
    {
        private readonly IEventStoreConnection _eventStoreConnection;

        public EventStoreReadiness(IEventStoreConnection eventStoreConnection)
        {
            _eventStoreConnection = eventStoreConnection;
        }

        public async Task<bool> IsReady()
        {
            try
            {
                await _eventStoreConnection.ReadEventAsync("foo", 0, false);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}