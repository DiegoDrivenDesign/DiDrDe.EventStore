﻿namespace DiDrDe.EventStore.Infra.EventStore
{
    public static class EventStoreConstants
    {
        public const string AggregateRootTypeHeader = "AggregateRootTypeHeader";
        public const string DomainEventTypeHeader = "DomainEventTypeHeader";
        public const string SerializableEventTypeHeader = "SerializableEventTypeHeader";
    }
}