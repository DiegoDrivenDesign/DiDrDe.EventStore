﻿using DiDrDe.EventStore.Infra.EventStore.Contracts.Factories;
using EventStore.ClientAPI;

namespace DiDrDe.EventStore.Infra.EventStore.Factories
{
    public class ExpectedVersionFactory
        : IExpectedVersionFactory
    {
        public long Create(int aggregateRootVersion, int numberOfEvents)
        {
            var originalVersion = aggregateRootVersion - numberOfEvents;
            var expectedVersion = originalVersion == 0
                ? ExpectedVersion.NoStream
                : originalVersion - 1;
            return expectedVersion;
        }
    }
}