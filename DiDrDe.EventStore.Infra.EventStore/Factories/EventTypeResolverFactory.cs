﻿using System;
using System.Collections.Generic;
using System.Linq;
using DiDrDe.EventStore.Infra.EventStore.Exceptions;
using DiDrDe.EventStore.Infra.EventStore.IoCC;

namespace DiDrDe.EventStore.Infra.EventStore.Factories
{
    public static class EventTypeResolverFactory
    {
        public static EventTypeResolver Create(
            IEnumerable<Type> existingTypes,
            IReadOnlyDictionary<Type, DomainEventSerializerDeserializer> domainEventSerializerDeserializers)
        {
            if (existingTypes == null) throw new ArgumentNullException(nameof(existingTypes));
            var types = new List<Type>(existingTypes);
            AddAdditionalTypes(types, domainEventSerializerDeserializers);
            var typesWithoutDuplicates = types.Distinct().ToList();
            ValidateUniqueness(typesWithoutDuplicates);
            var typeResolver = new EventTypeResolver(typesWithoutDuplicates);
            return typeResolver;
        }

        private static void ValidateUniqueness(IEnumerable<Type> typesWithoutDuplicates)
        {
            var typesWithSameClassName =
                            typesWithoutDuplicates
                                .GroupBy(x => x.Name)
                                .SelectMany(grp => grp.Skip(1))
                                .ToList();
            var areClassNamesNonUnique = typesWithSameClassName.Any();
            if (areClassNamesNonUnique)
            {
                throw new DuplicateEventNameException(typesWithSameClassName);
            }
        }

        private static void AddAdditionalTypes(
            IList<Type> types,
            IReadOnlyDictionary<Type, DomainEventSerializerDeserializer> domainEventSerializerDeserializers)
        {
            if (domainEventSerializerDeserializers is null)
            {
                return;
            }
            foreach (var keyValuePair in domainEventSerializerDeserializers)
            {
                var domainEventSerializerDeserializer = keyValuePair.Value;
                var domainEventType = domainEventSerializerDeserializer.DomainEventType;
                types.Add(domainEventType);
                var serializableEventType = domainEventSerializerDeserializer.SerializableEventType;
                types.Add(serializableEventType);
            }
        }
    }
}