﻿using System;
using DiDrDe.EventStore.Infra.EventStore.Contracts.Factories;

namespace DiDrDe.EventStore.Infra.EventStore.Factories
{
    public class StreamNameFactory
        : IStreamNameFactory
    {
        public string Create(Guid aggregateId, Type source)
        {
            var typeName = source.Name;
            var aggregateIdText = aggregateId.ToString();
            var idTrimmed = aggregateIdText.Replace("-", string.Empty);
            var result = $"{typeName}.{idTrimmed}";
            return result;
        }
    }
}