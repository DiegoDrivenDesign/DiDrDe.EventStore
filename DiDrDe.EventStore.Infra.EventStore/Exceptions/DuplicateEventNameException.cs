﻿using System;
using System.Collections.Generic;

namespace DiDrDe.EventStore.Infra.EventStore.Exceptions
{
    public class DuplicateEventNameException
        : Exception
    {
        public DuplicateEventNameException(IEnumerable<Type> eventTypes)
            : base($"Duplicate event names found {ToReadable(eventTypes)}")
        {
        }

        private static string ToReadable(IEnumerable<Type> eventTypes)
        {
            var readableEventTypeNames = String.Join(Environment.NewLine, eventTypes);
            return readableEventTypeNames;
        }
    }
}