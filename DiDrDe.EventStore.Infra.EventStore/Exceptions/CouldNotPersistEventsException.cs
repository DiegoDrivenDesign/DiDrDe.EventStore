﻿using System;

namespace DiDrDe.EventStore.Infra.EventStore.Exceptions
{
    public class CouldNotPersistEventsException
        : Exception
    {
        public CouldNotPersistEventsException(string message, Exception innerException)
            : base(message, innerException)
        {
        }
    }
}
