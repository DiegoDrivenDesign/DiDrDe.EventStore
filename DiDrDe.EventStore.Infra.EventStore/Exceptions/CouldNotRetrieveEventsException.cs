﻿using System;

namespace DiDrDe.EventStore.Infra.EventStore.Exceptions
{
    public class CouldNotRetrieveEventsException
        : Exception
    {
        public CouldNotRetrieveEventsException(string message, Exception innerException)
            : base(message, innerException)
        {
        }
    }
}
