﻿using DiDrDe.EventStore.Infra.EventStore.Contracts;
using EventStore.ClientAPI;

namespace DiDrDe.EventStore.Infra.EventStore
{
    public class RetrievedEvent
        : IRetrievedEvent
    {
        private readonly RecordedEvent _recordedEvent;

        public RetrievedEvent(ResolvedEvent resolvedEvent)
        {
            _recordedEvent = resolvedEvent.Event;
        }

        public byte[] Metadata
        {
            get
            {
                var metadata = _recordedEvent.Metadata;
                return metadata;
            }
        }

        public byte[] Data
        {
            get
            {
                var data = _recordedEvent.Data;
                return data;
            }
        }
    }
}