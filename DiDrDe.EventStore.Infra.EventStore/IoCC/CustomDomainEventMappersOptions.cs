﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using DiDrDe.CommonContracts;

namespace DiDrDe.EventStore.Infra.EventStore.IoCC
{
    public class CustomDomainEventMappersOptions
    {
        private readonly IDictionary<Type, DomainEventSerializerDeserializer> _domainEventSerializerDeserializers;

        public IReadOnlyDictionary<Type, DomainEventSerializerDeserializer> DomainEventSerializerDeserializers
        {
            get
            {
                var readOnlyDictionary = new ReadOnlyDictionary<Type, DomainEventSerializerDeserializer>(_domainEventSerializerDeserializers);
                return readOnlyDictionary;
            }
        }

        public CustomDomainEventMappersOptions()
        {
            _domainEventSerializerDeserializers = new Dictionary<Type, DomainEventSerializerDeserializer>();
        }

        public CustomDomainEventMappersOptions UsesCustomMappers<TDomainEvent, TSerializableEvent>(
            Func<TDomainEvent, TSerializableEvent> serializer,
            Func<TSerializableEvent, TDomainEvent> deserializer)
            where TDomainEvent : class, IDomainEvent
            where TSerializableEvent : class
        {
            var domainEventSerializerDeserializer = DomainEventSerializerDeserializer.Create(serializer, deserializer);
            var key = domainEventSerializerDeserializer.DomainEventType;
            _domainEventSerializerDeserializers.Add(key, domainEventSerializerDeserializer);
            return this;
        }
    }
}
