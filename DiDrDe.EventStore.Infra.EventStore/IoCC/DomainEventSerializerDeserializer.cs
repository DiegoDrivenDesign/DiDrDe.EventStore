﻿using System;
using DiDrDe.CommonContracts;

namespace DiDrDe.EventStore.Infra.EventStore.IoCC
{
    public class DomainEventSerializerDeserializer
    {
        public Type DomainEventType { get; }
        public Type SerializableEventType { get; }
        public Delegate Serializer { get; }
        public Delegate Deserializer { get; }

        public static DomainEventSerializerDeserializer Create<TDomainEvent, TSerializableEvent>(
            Func<TDomainEvent, TSerializableEvent> serializer,
            Func<TSerializableEvent, TDomainEvent> deserializer)
            where TDomainEvent : class, IDomainEvent
            where TSerializableEvent : class
        {
            var typeDomainEvent = typeof(TDomainEvent);
            var typeSerializableEvent = typeof(TSerializableEvent);
            var instance =
                new DomainEventSerializerDeserializer(
                    typeDomainEvent,
                    typeSerializableEvent,
                    serializer,
                    deserializer);
            return instance;
        }

        private DomainEventSerializerDeserializer(
            Type domainEventType,
            Type serializableEventType,
            Delegate serializer,
            Delegate deserializer)
        {
            DomainEventType = domainEventType ?? throw new ArgumentNullException(nameof(domainEventType));
            SerializableEventType = serializableEventType ?? throw new ArgumentNullException(nameof(serializableEventType));
            Serializer = serializer ?? throw new ArgumentNullException(nameof(serializer));
            Deserializer = deserializer ?? throw new ArgumentNullException(nameof(deserializer));
        }
    }
}